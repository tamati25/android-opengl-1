#! /bin/bash

# Version 1.0
# A simple script to remove trailing whitespaces
# using sed

function removeWhiteSpace()
{
    sed -i 's;\([ \|\t]\)*$;;g' "$1"
}

function doit()
{
    if [ $debug -eq 1 ]; then
	echo -e "\033[0;32mdoit $1\033[0m"
    fi

    for i in `find "$1" -type f`; do
	echo -e "\033[1;32mProcessing :\033[0m $i"
	removeWhiteSpace "$i"
    done

    echo -e "\033[1;32m[Done]\033[0m"
}

# Main function
if [ $# -eq 0 ]; then
    echo "Usage : $0 folder/file"
    exit 1
fi

file="$1"
doit "$file"
exit 0