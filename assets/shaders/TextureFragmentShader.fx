precision mediump float;

uniform sampler2D u_Texture;
varying vec4 v_Color;
varying vec2 v_TexCoordinate;

varying vec3 v_Position;

uniform int effect;
uniform vec3 camera;

vec4 blendColor(vec4 clrA, float clrAPerc, vec4 clrB, float clrBPerc);
float getSquaredDistance(vec3 A, vec3 B);
float clampBetween(float min, float value, float max);

//Effect 1 : B&W
void blackAndWhite()
{
	gl_FragColor = v_Color * texture2D(u_Texture, v_TexCoordinate);
	float avg = (gl_FragColor.r + gl_FragColor.g + gl_FragColor.b) / 3.0;

	if (avg < 0.2)
	{
		gl_FragColor.r = 1.0;
		gl_FragColor.g = 1.0;
		gl_FragColor.b = 1.0;
	}
	else
	{
		gl_FragColor.a = 0.0;
	}
}

//Effect 2 : fog
void fogEffect(vec3 camera)
{
	vec4 grey = vec4(0.8, 0.8, 0.8, 0.7);

	float invisible = 25.0 * 25.0;

	gl_FragColor = v_Color * texture2D(u_Texture, v_TexCoordinate); //get the "base color"

	float sqDist = getSquaredDistance(v_Position, camera);

	if (sqDist > invisible)
	{
		gl_FragColor.a = 0.02;
		return;
	}

	//Weighted Average, see http://sol.gfxile.net/interpolation/index.html
	float v = sqDist / invisible;
	float N = 6.0;
	float blendPerc = clampBetween(0.0, (v * (N - 1.0) + 1.0) / N, 1.0);

	gl_FragColor = blendColor(gl_FragColor, 1.0 - blendPerc, grey, blendPerc);
}

vec4 blendColor(vec4 clrA, float clrAPerc, vec4 clrB, float clrBPerc)
{
	vec4 result;

	result.r = clrA.r * clrAPerc + clrB.r * clrBPerc;
	result.g = clrA.g * clrAPerc + clrB.g * clrBPerc;
	result.b = clrA.b * clrAPerc + clrB.b * clrBPerc;
	result.a = clrA.a * clrAPerc + clrB.a * clrBPerc;

	return result;
}

float getSquaredDistance(vec3 A, vec3 B)
{
	return ((B.x - A.x) * (B.x - A.x) + (B.y - A.y) * (B.y - A.y) + (B.z - A.z) * (B.z - A.z));
}

float clampBetween(float min, float value, float max)
{
    if (value < min)    value = min;
    if (value > max)    value = max;

    return value;
}

void main()
{
	if (effect == 1)
	{
		blackAndWhite();
	}
	else if (effect == 2)
	{
		fogEffect(camera);
	}
	else
	{
		gl_FragColor = texture2D(u_Texture, v_TexCoordinate);
	}
}
