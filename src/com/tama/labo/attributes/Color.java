package com.tama.labo.attributes;

import java.util.Vector;

/**
 * Created by fpan on 15/09/14.
 */
public class Color extends Attribute
{
    public Color()
    {
        init(0, 0, 0, 0);
    }

    public Color(float r, float g, float b, float a)
    {
        init(r, g, b, a);
    }

    private void init(float r, float g, float b, float a)
    {
        data = new Vector<Float>(4);
        data.set(Attribute.RED, r);
        data.set(Attribute.GREEN, g);
        data.set(Attribute.BLUE, b);
        data.set(Attribute.ALPHA, a);
    }

    //Convert a color to its [0-1] equivalent
    public static Color getColor(int r, int g, int b, float a)
    {
        Color out = new Color();

        out.set(Attribute.RED,  r / 255.0f);
        out.set(Attribute.GREEN,  g / 255.0f);
        out.set(Attribute.BLUE,  b / 255.0f);
        out.set(Attribute.ALPHA,  a);

        return out;
    }

    @Override
    public Float[] toArray() {
        return data.toArray(new Float[4]);
    }
}
