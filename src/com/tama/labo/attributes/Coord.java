package com.tama.labo.attributes;

import java.util.Vector;

/**
 * Created by fpan on 15/09/14.
 */
public class Coord extends Attribute {
    public Coord()
    {
        init(0, 0);
    }

    public Coord(float x, float y)
    {
        init(x, y);
    }

    private void init(float x, float y)
    {
        data = new Vector<Float>(2);
        data.set(Attribute.XPOS, x);
        data.set(Attribute.YPOS, y);
    }

    @Override
    public Float[] toArray() {
        return data.toArray(new Float[2]);
    }
}
