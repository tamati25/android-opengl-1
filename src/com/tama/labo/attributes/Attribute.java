package com.tama.labo.attributes;

import com.tama.labo.interfaces.IAttrChanged;

import java.util.Vector;

/**
 * Created by fpan on 15/09/14.
 */
public abstract class Attribute {
    protected Vector<Float> data;

    //Coordinates indices
    public static final Integer XPOS = 0;
    public static final Integer YPOS = 1;
    public static final Integer ZPOS = 2;

    //Color indices
    public static final Integer RED = 0;
    public static final Integer GREEN = 1;
    public static final Integer BLUE = 2;
    public static final Integer ALPHA = 3;

    //Textures index
    public static final Integer TEX_U = 0;
    public static final Integer TEX_V = 1;

    // Code to execute when this attribute data is changed
    protected IAttrChanged attrChangedFunc;

    public IAttrChanged getAttrChangedFunc()
    {
        return attrChangedFunc;
    }

    public float get(int index)
    {
        return data.get(index);
    }

    public float set(int index, float value)
    {
        return data.set(index, value);
    }

    public abstract Float[] toArray();
}
