package com.tama.labo.attributes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by fpan on 15/09/14.
 */
public class ShapeData extends HashMap<Integer, Attribute>
{
    public static final Integer COLOR_DATA = 0;
    public static final Integer POSITION_DATA = 1;
    public static final Integer TEXTURE_DATA = 2;
    public static final Integer ADDITIONAL_DATA = 3;

    private List<Integer> indicesChanged;

    public ShapeData()
    {
        indicesChanged = new ArrayList<Integer>();
    }

    @Override
    public Attribute put(Integer index, Attribute attr)
    {
        super.put(index, attr);
        onDataChanged(index, attr);
        return attr;
    }

    private void onDataChanged(Integer index, Attribute attr)
    {
        indicesChanged.add(index);

        if (attr != null && attr.getAttrChangedFunc() != null)
            attr.getAttrChangedFunc().onDataChanged(index, attr);
    }

    /**
     * Determines the indices of the data that was changed, <b>and reset the list afterwards</b>
     * @return the indices of the data that changed since the last time this method was called
     */
    public List<Integer> getDataChanged()
    {
        List<Integer> result = new ArrayList<Integer>();
        for (int index : indicesChanged)
        {
            result.add(index);
        }
        indicesChanged.clear();
        return result;
    }

    public List<Integer> refreshAll()
    {
        List<Integer> result = new ArrayList<Integer>();
        for (int i : keySet())
        {
            result.add(i);
        }
        return result;
    }
}
