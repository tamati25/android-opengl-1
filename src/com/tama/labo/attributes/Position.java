package com.tama.labo.attributes;

import org.w3c.dom.Attr;

import java.util.Vector;

/**
 * Created by fpan on 15/09/14.
 */
public class Position extends Attribute {

    public Position()
    {
        init(0, 0, 0);
    }

    public Position(float x, float y, float z)
    {
        init(x, y, z);
    }

    private void init(float x, float y, float z)
    {
        data = new Vector<Float>(3);
        data.set(Attribute.XPOS, x);
        data.set(Attribute.YPOS, y);
        data.set(Attribute.ZPOS, z);
    }

    @Override
    public Float[] toArray() {
        return data.toArray(new Float[3]);
    }
}
