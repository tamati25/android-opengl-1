package com.tama.labo.attributes;

import java.util.Vector;

/**
 * Created by fpan on 15/09/14.
 */
public class Texture extends Attribute
{
    public Texture()
    {
        init(0, 0);
    }

    public Texture(float u, float v)
    {
        init(u, v);
    }

    private void init(float u, float v)
    {
        data = new Vector<Float>(2);
        data.set(Attribute.TEX_U, u);
        data.set(Attribute.TEX_V, v);
    }

    @Override
    public Float[] toArray() {
        return data.toArray(new Float[2]);
    }
}
