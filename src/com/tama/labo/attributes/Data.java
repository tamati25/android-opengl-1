package com.tama.labo.attributes;

/**
 * Created by fpan on 15/09/14.
 */
public class Data extends Attribute {
    Object data;

    public Data(Object data)
    {
        this.data = data;
    }

    public Object get()
    {
        return data;
    }

    @Override
    public Float[] toArray() {
        return null;
    }
}
