package com.tama.labo.shape;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.List;

import android.opengl.GLES20;

import com.tama.labo.game.Game;
import com.tama.labo.game.MainActivity;

public class MyShape extends Shape
{
    private final int BYTES_PER_FLOAT = 4;
    protected final int mStrideBytes = 7 * BYTES_PER_FLOAT;

	public MyShape() { super(); }

	@Override
	protected void refresh(List<Integer> indicesChanged)
	{
		vertexCount = 182;

		shapeCoords = new float[vertexCount * 7];

		shapeCoords[0] = 0.0f;
		shapeCoords[1] = -10.0f;
		shapeCoords[2] = 0;

		shapeCoords[3] = (float)(0x00 / 255.0);
		shapeCoords[4] = (float)(0x80 / 255.0);
		shapeCoords[5] = (float)(0x00 / 255.0);
		shapeCoords[6] = 1;

		//fill coordinates
		for (int i = 0; i <= 180; i ++)
		{
			double rad = (i  * 2) * Math.PI / 180;
			int index = 7 * (i + 1);
			shapeCoords[index] = (float) (2 * Math.cos(rad)); 	   //X
			shapeCoords[index + 1] = 0;							  //Y
			shapeCoords[index + 2] = (float) (2 * Math.sin(rad));  //Z

			shapeCoords[index + 3] = (float)(0x00/255.0);
			shapeCoords[index + 4] = (float)(Math.abs(Math.cos(rad) * 0xBF)/255.0);
			shapeCoords[index + 5] = (float)(Math.abs(Math.cos(rad) * 0xFF)/255.0);
			shapeCoords[index + 6] = 1;						  //A
		}


		ByteBuffer bb = ByteBuffer.allocateDirect(shapeCoords.length * 4);
		bb.order(ByteOrder.nativeOrder());

		vertexBuffer = bb.asFloatBuffer();
		vertexBuffer.put(shapeCoords);
		vertexBuffer.position(0);
	}

	@Override
	public void draw(MainActivity mainActivity, float[] mvpMatrix)
	{
		// Pass in the position information
        if (!shapeData.getDataChanged().isEmpty())
            refresh(shapeData.getDataChanged());

		vertexBuffer.position(mPositionOffset);
		GLES20.glVertexAttribPointer(mPositionHandle, mPositionDataSize, GLES20.GL_FLOAT, false,
									 mStrideBytes, vertexBuffer);

		GLES20.glEnableVertexAttribArray(mPositionHandle);

		// Pass in the color information
		vertexBuffer.position(mColorOffset);
		GLES20.glVertexAttribPointer(mColorHandle, mColorDataSize, GLES20.GL_FLOAT, false, mStrideBytes, vertexBuffer);

		GLES20.glEnableVertexAttribArray(mColorHandle);

		GLES20.glUniformMatrix4fv(mvpHandle, 1, false, mvpMatrix, 0);
		GLES20.glDrawArrays(GLES20.GL_TRIANGLE_FAN, 0, vertexCount);
	}
}
