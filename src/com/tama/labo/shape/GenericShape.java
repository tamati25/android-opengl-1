package com.tama.labo.shape;

import java.nio.ShortBuffer;
import java.util.List;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.Log;

import com.tama.labo.game.Game;
import com.tama.labo.game.MainActivity;
import com.tama.labo.parser.ObjParser;
import com.tama.labo.util.Constants;
import com.tama.labo.util.DebugUtil;

/*
 * Represents a generic shape (not abstract.)
 * This shape buffers are filled from the ObjParser class
 * and the shape is then drawn "normally".
 */
public class GenericShape extends Shape
{
	private ShortBuffer indexBuffer;
	private final ObjParser op;
	private int IBOLength;

	public GenericShape(String fileName, Context context, boolean isDownloadedFile)
	{
		super();

		// We do the parsing once, the shape is not supposed to move
		op = new ObjParser(fileName, context, isDownloadedFile);
		op.parse();

		refresh(shapeData.refreshAll());
	}

	@Override
	protected void refresh(List<Integer> indicesChanged)
	{
		Matrix.setIdentityM(mOwnMatrix, 0);

		vertexBuffer = op.getVertexBuffer();
		colorsBuffer = op.getColorBuffer();
		indexBuffer = op.getIndexBuffer();

		IBOLength = op.getBufferLength();

		if (DebugUtil.debugPrint(Constants.DEBUG_LEVEL_MORE_VERBOSE))
		{
			Log.d("GenericShape", DebugUtil.dumpFloatBuffer(vertexBuffer));
			Log.d("GenericShape", DebugUtil.dumpFloatBuffer(colorsBuffer));
			Log.d("GenericShape", DebugUtil.dumpShortBuffer(indexBuffer));
		}
	}

	@Override
	public void draw(MainActivity mainActivity, float[] mvpMatrix) {
		vertexBuffer.position(0);
		colorsBuffer.position(0);

		// Pass in the position information

		GLES20.glVertexAttribPointer(mPositionHandle, mPositionDataSize, GLES20.GL_FLOAT, false,
									 	12, vertexBuffer);

		GLES20.glEnableVertexAttribArray(mPositionHandle);

		//Pass in the color information
		GLES20.glVertexAttribPointer(mColorHandle, mColorDataSize, GLES20.GL_FLOAT, false, 16, colorsBuffer);

		GLES20.glEnableVertexAttribArray(mColorHandle);

		GLES20.glUniformMatrix4fv(mvpHandle, 1, false, mvpMatrix, 0);

		indexBuffer.position(0);
		GLES20.glDrawElements(GLES20.GL_TRIANGLE_STRIP, IBOLength, GLES20.GL_UNSIGNED_SHORT,
						indexBuffer);
	}

}
