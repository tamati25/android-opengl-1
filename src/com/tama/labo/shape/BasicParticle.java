package com.tama.labo.shape;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;
import java.util.List;

import android.opengl.GLES20;

import com.tama.labo.attributes.Attribute;
import com.tama.labo.attributes.Position;
import com.tama.labo.game.MainActivity;

public class BasicParticle extends Particle
{
	/* Default size : 0.5 */
	private float size = 0.5f;

	private ShortBuffer indexBuffer;

	/*
	 * --------------------------------
	 * Constructors
	 * --------------------------------
	 */
	public BasicParticle() { super(); }

	/*
	 * --------------------------------
	 * Shape abstract class implementation
	 * --------------------------------
	 */
	@Override
	protected void refresh(List<Integer> indicesChanged) {
		//vertexCount = 8;

		float posx = getPosX();
		float posy = getPosY();
		float posz = getPosZ();
		float red = getRed();
		float green = getGreen();
		float blue = getBlue();
		float alpha = getAlpha();

		shapeCoords = new float[]{//front face
								posx, posy, posz + size,
								posx + size, posy, posz + size,
								posx, posy + size, posz + size,
								posx + size, posy + size, posz + size,

								//back face
								posx, posy, posz,
								posx + size, posy, posz,
								posx, posy + size, posz,
								posx + size, posy + size, posz};

		shapeColors = new float[]{ // make the front a little more transparent
								 red, green, blue, alpha,
								 red, green, blue, alpha,
								 red, green, blue, alpha,
								 red, green, blue, alpha,
								 red, green, blue, alpha,
								 red, green, blue, alpha,
								 red, green, blue, alpha,
								 red, green, blue, alpha,
								};

		vertexBuffer = ByteBuffer.allocateDirect(shapeCoords.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
		vertexBuffer.clear();
		vertexBuffer.put(shapeCoords);

		colorsBuffer = ByteBuffer.allocateDirect(shapeColors.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
		colorsBuffer.clear();
		colorsBuffer.put(shapeColors);

		/*
		 *   6-----------7
		 *  /|          /|
		 * 2-----------3 |
		 * | |         | |
		 * | |         | |
		 * | 4---------|-5
		 * |/          |/
		 * 0-----------1      ... see my ascii skills.
		 */

		/*
		 * Index buffer :
		 * 	 /!\ Culling : Only the triangles with indices *counter-clockwise* will
		 * 				   be displayed
		 * 				   i.e  {3, 0, 2} -> nope ; {0, 3, 2} -> yay
		 *
		 *   /!\ Triangle strip order : {v0, v1, v2} {v2, v1, v3} {v2, v3, v4} ...
		 */
		short[] indices = {0, 1, 2, 3, 7, 1, 5, 4, 7, 6, 2, 4, 0, 1	};

		indexBuffer = ByteBuffer.allocateDirect(indices.length * 2)
				.order(ByteOrder.nativeOrder())
				.asShortBuffer();
		indexBuffer.put(indices);
	}

	@Override
	public void draw(MainActivity mainActivity, float[] mvpMatrix) {

        if (!shapeData.getDataChanged().isEmpty())
		    refresh(shapeData.getDataChanged());

		// Pass in the position information
		vertexBuffer.position(0);

		GLES20.glVertexAttribPointer(mPositionHandle, mPositionDataSize, GLES20.GL_FLOAT, false,
									 12, vertexBuffer);

		GLES20.glEnableVertexAttribArray(mPositionHandle);

		// Pass in the color information
		colorsBuffer.position(0);
		GLES20.glVertexAttribPointer(mColorHandle, mColorDataSize, GLES20.GL_FLOAT, false, 16, colorsBuffer);

		GLES20.glEnableVertexAttribArray(mColorHandle);

		GLES20.glUniformMatrix4fv(mvpHandle, 1, false, mvpMatrix, 0);

		indexBuffer.position(0);
		GLES20.glDrawElements(GLES20.GL_TRIANGLE_STRIP, 14, GLES20.GL_UNSIGNED_SHORT, indexBuffer);
	}

	public Position getCenterPos()
	{
		Position result = new Position();

		float size = getSize() / 2;

        result.set(Attribute.XPOS, getPosX() + size);
        result.set(Attribute.YPOS, getPosY() + size);
        result.set(Attribute.ZPOS, getPosZ() + size);

		return result;
	}

	public float squareDistanceTo(Particle b)
	{
		Position myPos = getPos();
		Position bPos = b.getPos();

        float myX = myPos.get(Attribute.XPOS);
        float myY = myPos.get(Attribute.YPOS);
        float myZ = myPos.get(Attribute.ZPOS);

        float itsX = bPos.get(Attribute.XPOS);
        float itsY = bPos.get(Attribute.YPOS);
        float itsZ = bPos.get(Attribute.ZPOS);

        return (float) Math.sqrt(Math.abs(myX - itsX) * Math.abs(myX - itsX) +
                Math.abs(myY - itsY) * Math.abs(myY - itsY) + Math.abs(myZ - itsZ) * Math.abs(myZ - itsZ));
	}
}
