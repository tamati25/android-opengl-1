package com.tama.labo.shape;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.List;

import android.opengl.GLES20;
import android.util.Log;

import com.tama.labo.attributes.Attribute;
import com.tama.labo.attributes.Color;
import com.tama.labo.attributes.Position;
import com.tama.labo.game.MainActivity;

public class TexturedField extends Shape
{
	private int fieldWidth = 50;
	private int fieldHeight = 50;
	private float squareSize = 1.0f;

	private int textureId;
	private float[] heightMap = null;

	private FloatBuffer texCoordBuffer;

	private boolean hasChanged = false;

	/* Constructors */

	public TexturedField(int textureId)
	{
		super();
		setTextureId(textureId);
	}


	/* Getters/setters */

	public TexturedField setTextureId(int textureId)
	{
		this.textureId = textureId;
		hasChanged = true;
		return this;
	}

	public int getTextureId() {
		return textureId;
	}

	public TexturedField setHeightMap(float[] heightMap)
	{
		int len = heightMap.length;
		int expectedLen = fieldHeight * fieldWidth;

		if (len != expectedLen)
			Log.w("TexturedField", "Invalid size : expected " + expectedLen + ", got " + len);
		else
			this.heightMap = heightMap;

		return this;
	}

	public float[] getHeightMap()
	{
		return heightMap;
	}

	/* Inherited methods */

	public int getFieldWidth() {
		return fieldWidth;
	}

	public TexturedField setFieldWidth(int fieldWidth) {
		this.fieldWidth = fieldWidth;
		return this;
	}

	public int getFieldHeight() {
		return fieldHeight;
	}

	public TexturedField setFieldHeight(int fieldHeight) {
		this.fieldHeight = fieldHeight;
		return this;
	}

	protected float heightAt(int row, int col)
	{
		return heightMap[row * fieldWidth + col];
	}

	@Override
	protected void refresh(List<Integer> indicesChanged)
	{
		final int numberOfSquares = 6 * fieldWidth * fieldHeight;
		final int elementsPerSquare = 6;

		float coords[] = new float[elementsPerSquare * numberOfSquares * 3];
		float colors[] = new float[elementsPerSquare * numberOfSquares * 4];
		float texCoords[] = new float[elementsPerSquare * numberOfSquares * 2];

		if (heightMap == null)
		{
			Log.d("TexturedField", "No height map given, generating default one");

			heightMap = new float[fieldHeight];

			for (int row = 0; row < fieldHeight; ++row)
			{
				heightMap[row] = row;
			}
		}

		for (int row = 0; row < fieldHeight - 1; ++row)
		{
			for (int col = 0; col < fieldWidth - 1; ++col)
			{
				int indice = 6 * (row * fieldWidth + col);

				/* Aliases */
                Color c = getColor();
				float r = c.get(Attribute.RED);
				float g = c.get(Attribute.GREEN);
				float b = c.get(Attribute.BLUE);
				float a = c.get(Attribute.ALPHA);

				/*
				 *                Col      Col + 1
				 *                 A         C
				 *  Row + 1         *--------*
				 *                 /        /
				 *                /        /        => {A, B, C, B, D, C}
				 *  Row          *--------*
				 *              B        D
				 *
				 */

                Position p = getPos();
				Position A = new Position(p.get(Attribute.XPOS) + col * squareSize,
                        p.get(Attribute.YPOS) + heightAt(row + 1, col), p.get(Attribute.ZPOS) - (row + 1) * squareSize);
				Position B = new Position(p.get(Attribute.XPOS) + col * squareSize,
                        p.get(Attribute.YPOS) + heightAt(row, col), p.get(Attribute.ZPOS) - row * squareSize);
				Position C = new Position(p.get(Attribute.XPOS) + (col + 1) * squareSize,
                        p.get(Attribute.YPOS) + heightAt(row + 1, col + 1), p.get(Attribute.ZPOS) - (row + 1) * squareSize);
				Position D = new Position(p.get(Attribute.XPOS) + (col + 1) * squareSize,
                        p.get(Attribute.YPOS) + heightAt(row, col + 1), p.get(Attribute.ZPOS) - row * squareSize);

				float localCoords[] = {
						A.get(Attribute.XPOS), A.get(Attribute.YPOS), A.get(Attribute.ZPOS),
						B.get(Attribute.XPOS), B.get(Attribute.YPOS), B.get(Attribute.ZPOS),
						C.get(Attribute.XPOS), C.get(Attribute.YPOS), C.get(Attribute.ZPOS),
						B.get(Attribute.XPOS), B.get(Attribute.YPOS), B.get(Attribute.ZPOS),
						D.get(Attribute.XPOS), D.get(Attribute.YPOS), D.get(Attribute.ZPOS),
						C.get(Attribute.XPOS), C.get(Attribute.YPOS), C.get(Attribute.ZPOS),
				};

				float localColors[] = {
						r, g, b, a,
						r, g, b, a,
						r, g, b, a,
						r, g, b, a,
						r, g, b, a,
						r, g, b, a,
				};

				float colF = col;
                float rowF = row;

				float localTexCoords[] = {
						colF/fieldWidth, (rowF + 1) / fieldHeight,
						colF/fieldWidth, rowF / fieldHeight,
						(colF + 1)/fieldWidth, (rowF + 1) / fieldHeight,
						colF/fieldWidth, rowF / fieldHeight,
						(colF + 1)/fieldWidth, rowF / fieldHeight,
						(colF + 1)/fieldWidth, (rowF + 1) / fieldHeight,
				};
				//copy local arrays to the corresponding arrays
				System.arraycopy(localCoords, 0, coords, 3 * indice, 18);
				System.arraycopy(localColors, 0, colors, 4 * indice, 24);
				System.arraycopy(localTexCoords, 0, texCoords, 2 * indice, 12);
			}
		}

		//Create buffers
		vertexBuffer = ByteBuffer.allocateDirect(coords.length * 4)
				.order(ByteOrder.nativeOrder()).asFloatBuffer();
		vertexBuffer.clear();
		vertexBuffer.put(coords);

		colorsBuffer = ByteBuffer.allocateDirect(colors.length * 4)
				.order(ByteOrder.nativeOrder()).asFloatBuffer();
		colorsBuffer.clear();
		colorsBuffer.put(colors);

		texCoordBuffer = ByteBuffer.allocateDirect(texCoords.length * 4)
				.order(ByteOrder.nativeOrder()).asFloatBuffer();
		texCoordBuffer.clear();
		texCoordBuffer.put(texCoords);
	}

	@Override
	public void draw(MainActivity activity, float[] mvpMatrix)
	{
        if (!shapeData.getDataChanged().isEmpty())
            refresh(shapeData.getDataChanged());

        int mProgram = s.getmProgram();
        int mTextureUniformHandle = GLES20.glGetUniformLocation(mProgram, "u_Texture");
        int mTextureHandle = GLES20.glGetAttribLocation(mProgram, "a_TexCoordinate");
        int mTextureDataSize = 2; /* 2 elements (S, T) */


        int mEffectHandle = GLES20.glGetUniformLocation(mProgram, "effect");
        int effect = 2;
        GLES20.glUniform1i(mEffectHandle, effect);

        //Get camera position in world space
        float[] camera = new float[] {0.0f, 0.0f, 0.0f};

        int mCameraHandle = GLES20.glGetUniformLocation(mProgram, "camera");
        GLES20.glUniform3f(mCameraHandle, camera[0], camera[1], camera[2]);


		GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureId);
		GLES20.glUniform1i(mTextureUniformHandle, 0);

        vertexBuffer.position(0);
        colorsBuffer.position(0);
        texCoordBuffer.position(0);

        GLES20.glVertexAttribPointer(mPositionHandle, mPositionDataSize, GLES20.GL_FLOAT, false, 12, vertexBuffer);
		GLES20.glEnableVertexAttribArray(mPositionHandle);

		GLES20.glVertexAttribPointer(mColorHandle, mColorDataSize, GLES20.GL_FLOAT, false, 16, colorsBuffer);
		GLES20.glEnableVertexAttribArray(mColorHandle);

		GLES20.glVertexAttribPointer(mTextureHandle, mTextureDataSize, GLES20.GL_FLOAT, false, 8, texCoordBuffer);
		GLES20.glEnableVertexAttribArray(mTextureHandle);

		// Draw !
		GLES20.glUniformMatrix4fv(mvpHandle, 1, false, mvpMatrix, 0);
		GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, 6 * fieldWidth * fieldHeight);
	}
}
