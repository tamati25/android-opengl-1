package com.tama.labo.shape;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.Arrays;
import java.util.List;

import android.opengl.GLES20;
import android.util.Log;

import com.tama.labo.attributes.ShapeData;
import com.tama.labo.engine.Engine;
import com.tama.labo.engine.Engine3;
import com.tama.labo.game.MainActivity;
import com.tama.labo.manager.Managers;
import com.tama.labo.util.Constants;
import com.tama.labo.util.DebugUtil;

public class TexturedParticle extends Particle
{
	private FloatBuffer texCoordBuffer;

	private final Integer[] textures = new Integer[6];

	//Order in which the faces are defined, used to set a face texture more easily
	public static final int FRONT_FACE = 0;
	public static final int TOP_FACE = 1;
	public static final int BACK_FACE = 2;
	public static final int BOTTOM_FACE = 3;
	public static final int LEFT_FACE = 4;
	public static final int RIGHT_FACE = 5;

    public TexturedParticle()
    {
        super();

        /*
		 * The textures coordinates should not change.
		 */
        int numberOfTexCoordElements = 2 * 6 * 6;
        float[] texCoords = new float[numberOfTexCoordElements];
        for (int i = 0 ; i < numberOfTexCoordElements; i += 12)
        {
            texCoords[i] = 0f;
            texCoords[i + 1] = 0f;
            texCoords[i + 2] = 0f;
            texCoords[i + 3] = 1f;
            texCoords[i + 4] = 1f;
            texCoords[i + 5] = 0f;
            texCoords[i + 6] = 0f;
            texCoords[i + 7] = 1f;
            texCoords[i + 8] = 1f;
            texCoords[i + 9] = 1f;
            texCoords[i + 10] = 1f;
            texCoords[i + 11] = 0f;
        }


        texCoordBuffer = ByteBuffer.allocateDirect(texCoords.length * 4)
                .order(ByteOrder.nativeOrder()).asFloatBuffer();
        texCoordBuffer.clear();
        texCoordBuffer.put(texCoords);
    }

	public TexturedParticle setTextures(Managers managers)
	{
		Integer defaultTexture = managers.getTextureManager().getDefault();
		Log.d("TexturedParticle", "Setting default particle (id = " + defaultTexture + ")");

		for (int i = 0; i < 6; ++i)
			this.textures[i] = defaultTexture;

        return this;
	}

    public TexturedParticle setUniformTexture(int textureId)
    {
        for (int i = 0; i < 6; ++i)
            this.textures[i] = textureId;

        return this;
    }

	public TexturedParticle setTextures(Managers managers, int[] textures)
	{
		int texturesLen = Math.min(textures.length, 6);

		//Textures must be an array of 6 elements (1 texture per side)
		//Missing sides will be filled with default texture

		for (int i = 0; i < texturesLen; ++i)
			this.textures[i] = textures[i];

		if (texturesLen < 6)
		{
			Log.w("TexturedParticle", "Not enough textures given, filling missing with default texture");
			Integer defaultTexture = managers.getTextureManager().getDefault();
			for (int i = texturesLen; i < 6; ++i)
				this.textures[i] = defaultTexture;
		}

        return this;
	}

	public TexturedParticle setTexture(int side, int texId)
	{
		if (side < 0 || side > 5)
		{
			Log.w("TexturedParticle", "Invalid index");
			return this;
		}

		textures[side] = texId;

        return this;
	}

	public Integer getTexture(int side)
	{
		if (side < 0 || side > 5)
		{
			Log.w("TexturedParticle", "Invalid index");
			return null;
		}

		return textures[side];
	}

	@Override
	protected void refresh(List<Integer> indicesChanged)
	{
        if (indicesChanged.contains(ShapeData.POSITION_DATA))
        {
            float posx = getPosX();
            float posy = getPosY();
            float posz = getPosZ();

            // The position given is the coordinates of the lower-left vertex
            // in the back.

            float[] coords =
                    {
                            //Front face
                            posx, posy + size, posz,
                            posx, posy, posz,
                            posx + size, posy + size, posz,
                            posx, posy, posz,
                            posx + size, posy, posz,
                            posx + size, posy + size, posz,

                            //Top face
                            posx, posy + size, posz - size,
                            posx, posy + size, posz,
                            posx + size, posy + size, posz - size,
                            posx, posy + size, posz,
                            posx + size, posy + size, posz,
                            posx + size, posy + size, posz - size,

                            //Back face
                            posx + size, posy + size, posz - size,
                            posx + size, posy, posz - size,
                            posx, posy + size, posz - size,
                            posx + size, posy, posz - size,
                            posx, posy, posz - size,
                            posx, posy + size, posz - size,

                            //Bottom face
                            posx + size, posy, posz - size,
                            posx + size, posy, posz,
                            posx, posy, posz - size,
                            posx + size, posy, posz,
                            posx, posy, posz,
                            posx, posy, posz - size,

                            //Left face
                            posx, posy + size, posz - size,
                            posx, posy, posz - size,
                            posx, posy + size, posz,
                            posx, posy, posz - size,
                            posx, posy, posz,
                            posx, posy + size, posz,

                            //Right face
                            posx + size, posy + size, posz - size,
                            posx + size, posy, posz - size,
                            posx + size, posy + size, posz,
                            posx + size, posy, posz - size,
                            posx + size, posy, posz,
                            posx + size, posy + size, posz,
                    };

            vertexBuffer = ByteBuffer.allocateDirect(coords.length * 4)
                    .order(ByteOrder.nativeOrder()).asFloatBuffer();
            vertexBuffer.clear();
            vertexBuffer.put(coords);
        }

        if (indicesChanged.contains(ShapeData.COLOR_DATA))
        {
            //To keep it simple, each vertice have the same color, get with the
            //appropriate getters (see Particle class)
            int numberOfColorElements = 36;
            float[] colors = new float[numberOfColorElements * 4];
            for (int i = 0; i < numberOfColorElements; i++) {
                int indice = 4 * i;
                colors[indice] = getRed();
                colors[indice + 1] = getGreen();
                colors[indice + 2] = getBlue();
                colors[indice + 3] = getAlpha();
            }


            colorsBuffer = ByteBuffer.allocateDirect(colors.length * 4)
                    .order(ByteOrder.nativeOrder()).asFloatBuffer();
            colorsBuffer.clear();
            colorsBuffer.put(colors);
        }
	}

	@Override
	public void draw(MainActivity mainActivity, float[] mvpMatrix)
	{
        if (!shapeData.getDataChanged().isEmpty())
            refresh(shapeData.getDataChanged());

        int mProgram = s.getmProgram();
        int mTextureUniformHandle = GLES20.glGetUniformLocation(mProgram, "u_Texture");
        int mTextureHandle = GLES20.glGetAttribLocation(mProgram, "a_TexCoordinate");
        int mTextureDataSize = 2; /* 2 elements (S, T) */

        int mEffectHandle = GLES20.glGetUniformLocation(mProgram, "effect");

        int effect = 0;
        Engine currentEngine = mainActivity.getGame().getEngine();

        if (currentEngine instanceof Engine3)
        {
            effect = ((Engine3)currentEngine).getEffect();
            DebugUtil.logD("TexturedParticle", "effect = " + effect, Constants.DEBUG_LEVEL_MORE_VERBOSE);
        }

        vertexBuffer.position(0);
        colorsBuffer.position(0);
        texCoordBuffer.position(0);

		for (int i = 0; i < 6; ++i)
		{
			GLES20.glUniform1i(mTextureUniformHandle, textures[i]);

			GLES20.glVertexAttribPointer(mPositionHandle, mPositionDataSize, GLES20.GL_FLOAT, false, 12, vertexBuffer);
			GLES20.glEnableVertexAttribArray(mPositionHandle);

			GLES20.glVertexAttribPointer(mColorHandle, mColorDataSize, GLES20.GL_FLOAT, false, 16, colorsBuffer);
			GLES20.glEnableVertexAttribArray(mColorHandle);

			GLES20.glVertexAttribPointer(mTextureHandle, mTextureDataSize, GLES20.GL_FLOAT, false, 8, texCoordBuffer);
			GLES20.glEnableVertexAttribArray(mTextureHandle);

	        GLES20.glUniform1i(mEffectHandle, effect);

			// Draw !
			GLES20.glUniformMatrix4fv(mvpHandle, 1, false, mvpMatrix, 0);
			GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 6 * i, 6);
		}
	}
}
