package com.tama.labo.shape;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.List;

import android.opengl.GLES20;

import com.tama.labo.attributes.Attribute;
import com.tama.labo.attributes.Color;
import com.tama.labo.attributes.Position;
import com.tama.labo.game.MainActivity;

public class Picture extends Shape
{
    private ShortBuffer indexBuffer;
	private FloatBuffer texCoordBuffer;

	private int textureId = 0;
	private Position position = new Position(0, 0, 0);
	private Color color = new Color(1, 1, 1, 1);
	private float size = 1.0f;

	public Picture(int textureId)
	{
		super();
		this.textureId = textureId;
	}

	/*
	 * Get and set attributes
	 * The setters return the Picture modified so that we are able to chain them, i.e
	 *   Picture p = new Picture(myTexture);
	 *   p.setPosition(position).setColor(color).setSize(size);
	 */
	public int getTextureId() {
		return textureId;
	}

	public Picture setTextureId(int textureId) {
		this.textureId = textureId;
		return this;
	}

	/* Implement inherited methods */
	@Override
	protected void refresh(List<Integer> indicesChanged) {
		float posx = position.get(Attribute.XPOS);
		float posy = position.get(Attribute.YPOS);
		float posz = position.get(Attribute.ZPOS);

		// The position given is the coordinates of the lower-left vertex
		// in the back.

		float[] coords =
			{
				//Front face
                posx, posy, posz,
                posx + size, posy, posz,
                posx + size, posy + size, posz,
                posx, posy + size, posz
			};

        short[] index = {0, 1, 2, 3, 0, 2};

		int numberOfColorElements = 6;
		float[] colors = new float[numberOfColorElements * 4];
		for (int i = 0; i < numberOfColorElements; i++)
		{
			int indice = 4 * i;
			colors[indice] = color.get(Attribute.RED);
			colors[indice + 1] = color.get(Attribute.GREEN);
			colors[indice + 2] = color.get(Attribute.BLUE);
			colors[indice + 3] = color.get(Attribute.ALPHA);
		}

		float[] texCoords =
			{
				0f, 0f,
				0f, 1f,
				1f, 0f,
				0f, 1f,
				1f, 1f,
				1f, 0f,
			};

		vertexBuffer = ByteBuffer.allocateDirect(coords.length * 4)
				.order(ByteOrder.nativeOrder()).asFloatBuffer();
		vertexBuffer.clear();
		vertexBuffer.put(coords);

        indexBuffer = ByteBuffer.allocateDirect(index.length * 2)
                .order(ByteOrder.nativeOrder()).asShortBuffer();
        indexBuffer.clear();
        indexBuffer.put(index);

		colorsBuffer = ByteBuffer.allocateDirect(colors.length * 4)
				.order(ByteOrder.nativeOrder()).asFloatBuffer();
		colorsBuffer.clear();
		colorsBuffer.put(colors);

		texCoordBuffer = ByteBuffer.allocateDirect(texCoords.length * 4)
				.order(ByteOrder.nativeOrder()).asFloatBuffer();
		texCoordBuffer.clear();
		texCoordBuffer.put(texCoords);
	}

	@Override
	public void draw(MainActivity mainActivity, float[] mvpMatrix)
	{
        if (!shapeData.getDataChanged().isEmpty())
            refresh(shapeData.getDataChanged());

        int mProgram = s.getmProgram();
        int mTextureUniformHandle = GLES20.glGetUniformLocation(mProgram, "u_Texture");
        int mTextureHandle = GLES20.glGetAttribLocation(mProgram, "a_TexCoordinate");
        int mTextureDataSize = 2; /* 2 elements (S, T) */

        vertexBuffer.position(0);
        indexBuffer.position(0);
        colorsBuffer.position(0);
        texCoordBuffer.position(0);

        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureId);
        GLES20.glUniform1i(mTextureUniformHandle, 0);

        GLES20.glVertexAttribPointer(mPositionHandle, mPositionDataSize, GLES20.GL_FLOAT, false, 12, vertexBuffer);
        GLES20.glEnableVertexAttribArray(mPositionHandle);

        GLES20.glVertexAttribPointer(mColorHandle, mColorDataSize, GLES20.GL_FLOAT, false, 16, colorsBuffer);
        GLES20.glEnableVertexAttribArray(mColorHandle);

        GLES20.glUniformMatrix4fv(mvpHandle, 1, false, mvpMatrix, 0);
        GLES20.glDrawElements(GLES20.GL_TRIANGLES, 6, GLES20.GL_UNSIGNED_SHORT, indexBuffer);
    }
}
