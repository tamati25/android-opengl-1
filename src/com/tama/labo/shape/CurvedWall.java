package com.tama.labo.shape;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;
import java.util.List;

import android.opengl.GLES20;
import android.opengl.Matrix;

import com.tama.labo.attributes.Attribute;
import com.tama.labo.attributes.Position;
import com.tama.labo.game.MainActivity;

public class CurvedWall extends Shape
{
	private float posx = 0; //x position of the top-left corner (default 0)
	private float posy = 0; //y position of the top-left corner (default 0)
	//private float posz = 0; //z position of the top-left corner (default 0)

    //private float curveMax = 50; //curve reached at the middle of the wall (default 50)

	private ShortBuffer indexBuffer;

	private final int WIDTH_SQ_COUNT = 100;
	private final int HEIGHT_SQ_COUNT = 20;

    /* Constructors */
	public CurvedWall(Position p)
	{
		//p is the top-left corner
		super();

        this.setPos(p);
		this.posx = p.get(Attribute.XPOS);
		this.posy = p.get(Attribute.YPOS);
		//this.posz = p.z;
	}

	/* Util */
	private short[] buildIBO()
	{
		//4 * WIDTH_SQ_COUNT triangles on each row, and HEIGHT_SQ_COUNT - 1 rows
        short trianglesPerRow = 4 * (WIDTH_SQ_COUNT - 1);
        short shapeIndex[] = new short[trianglesPerRow * (HEIGHT_SQ_COUNT - 1)];

		for (int row = 0; row < HEIGHT_SQ_COUNT - 1; ++row)
		{
			for (int i = 0; i < WIDTH_SQ_COUNT - 1; ++i)
			{
				int indice = trianglesPerRow * row + 4 * i;
				shapeIndex[indice] = (short) (row * WIDTH_SQ_COUNT + i);
				shapeIndex[indice + 1] = (short) (row * WIDTH_SQ_COUNT + i + 1);
				shapeIndex[indice + 2] = (short) ((row + 1) * WIDTH_SQ_COUNT + i);
				shapeIndex[indice + 3] = (short) ((row + 1) * WIDTH_SQ_COUNT + i + 1);
			}
		}

		return shapeIndex;
	}

	@Override
	protected void refresh(List<Integer> indicesChanged)
	{
		Matrix.setIdentityM(mOwnMatrix, 0);

        int size = WIDTH_SQ_COUNT * HEIGHT_SQ_COUNT;
        float shapeCoords[] = new float[size * 3];
		float shapeColors[] = new float[size * 4];

		for (int j = 0 ; j < HEIGHT_SQ_COUNT ; ++j)
		{
			for (int i = 0 ; i < WIDTH_SQ_COUNT; ++i)
			{
				int indice = WIDTH_SQ_COUNT * j + i;

				 //Coordinates
                float unitSize = 0.1f;
                shapeCoords[3 * indice] = posx + unitSize * i;
				shapeCoords[3 * indice + 1] = posy + (unitSize * 4) * j;
				//shapeCoords[3 * indice + 2] = (float) (posz - MathUtil.getCurve(j, curveMax, 10));
				shapeCoords[3 * indice + 2] = 0;

                float red = 0;
                shapeColors[4 * indice] = red;
                float green = 0;
                shapeColors[4 * indice + 1] = green;
                float blue = 1;
                shapeColors[4 * indice + 2] = blue;
                float alpha = 1f;
                shapeColors[4 * indice + 3] = alpha;
			}
		}

		vertexBuffer = ByteBuffer.allocateDirect(shapeCoords.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
		vertexBuffer.clear();
		vertexBuffer.put(shapeCoords);

		colorsBuffer = ByteBuffer.allocateDirect(shapeColors.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
		colorsBuffer.clear();
		colorsBuffer.put(shapeColors);

		short[] shapeIndex = buildIBO();
		indexBuffer = ByteBuffer.allocateDirect(shapeIndex.length * 2)
					.order(ByteOrder.nativeOrder())
					.asShortBuffer();
		indexBuffer.clear();
		indexBuffer.put(shapeIndex);
		indexBuffer.position(0);
	}

	@Override
	public void draw(MainActivity mainActivity, float[] mvpMatrix)
	{
        if (!shapeData.getDataChanged().isEmpty())
            refresh(shapeData.getDataChanged());

		vertexBuffer.position(0);
		colorsBuffer.position(0);

		// Pass in the position information

		GLES20.glVertexAttribPointer(mPositionHandle, mPositionDataSize, GLES20.GL_FLOAT, false,
									 	12, vertexBuffer);

		GLES20.glEnableVertexAttribArray(mPositionHandle);

		//Pass in the color information
		GLES20.glVertexAttribPointer(mColorHandle, mColorDataSize, GLES20.GL_FLOAT, false, 16, colorsBuffer);

		GLES20.glEnableVertexAttribArray(mColorHandle);

		GLES20.glUniformMatrix4fv(mvpHandle, 1, false, mvpMatrix, 0);

		indexBuffer.position(0);
        int numberOfTriangles = 4 * (WIDTH_SQ_COUNT - 1) * (HEIGHT_SQ_COUNT - 1);
        GLES20.glDrawElements(GLES20.GL_TRIANGLE_STRIP, numberOfTriangles, GLES20.GL_UNSIGNED_SHORT,
						indexBuffer);
	}
}
