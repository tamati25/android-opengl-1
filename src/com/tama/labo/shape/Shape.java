package com.tama.labo.shape;

import java.nio.FloatBuffer;
import java.util.List;

import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.Log;

import com.tama.labo.attributes.Color;
import com.tama.labo.attributes.Data;
import com.tama.labo.attributes.Position;
import com.tama.labo.attributes.ShapeData;
import com.tama.labo.game.MainActivity;
import com.tama.labo.manager.Managers;
import com.tama.labo.manager.ShaderManager;
import com.tama.labo.shader.Shader;
import com.tama.labo.util.Constants;
import com.tama.labo.util.DebugUtil;

import static com.tama.labo.attributes.ShapeData.*;
import static com.tama.labo.attributes.Attribute.*;

public abstract class Shape
{
    private static final String DEFAULT_SHADER_NAME = "BasicShader";

	protected String shaderName = null;
	protected Shader s = null; //the initialize method will set it correctly

	protected int mPositionHandle;
	protected int mColorHandle;
	protected int mvpHandle;

    protected final int mColorDataSize = 4;
    protected final int mPositionOffset = 0;
    protected final int mPositionDataSize = 3;
    protected final int mColorOffset = 3;

	protected FloatBuffer vertexBuffer;
	protected FloatBuffer colorsBuffer;

	protected float[] shapeCoords;
	protected float[] shapeColors;

	//Shape common attributes
    protected ShapeData shapeData;
	protected float size;
	
	//Position related methods
    public void setPos(Position p) { shapeData.put(POSITION_DATA, p); }
    public void setPosX(float x) { Position p = getPos(); p.set(XPOS, x); shapeData.put(POSITION_DATA, p); }
    public void setPosY(float y) { Position p = getPos(); p.set(YPOS, y); shapeData.put(POSITION_DATA, p); }
    public void setPosZ(float z) { Position p = getPos(); p.set(ZPOS, z); shapeData.put(POSITION_DATA, p); }

	public Position getPos() { return (Position) shapeData.get(POSITION_DATA);}
	public float getPosX() { return ((Position) shapeData.get(POSITION_DATA)).get(XPOS); }
	public float getPosY() { return ((Position) shapeData.get(POSITION_DATA)).get(YPOS); }
	public float getPosZ() { return ((Position) shapeData.get(POSITION_DATA)).get(ZPOS); }

	// Size related methods
	public void setSize(float pSize) { size = pSize; }
	public float getSize() { return size; }
	
	//Color related methods
	public void setColor(Color c) { shapeData.put(COLOR_DATA, c); }
	public void setColor(float r, float g, float b, float a)
	{
        Color c = new Color(r, g, b, a);
        setColor(c);
	}
	public void setRed(float red) { Color c = getColor(); c.set(RED, red); setColor(c); }
	public void setBlue(float blue) { Color c = getColor(); c.set(BLUE, blue); setColor(c); }
	public void setGreen(float green) { Color c = getColor(); c.set(GREEN, green); setColor(c); }
	public void setAlpha(float alpha) { Color c = getColor(); c.set(ALPHA, alpha); setColor(c); }
	public Color getColor() { return (Color) shapeData.get(COLOR_DATA); }
	public float getRed() { return ((Color) shapeData.get(COLOR_DATA)).get(RED); }
	public float getBlue() { return ((Color) shapeData.get(COLOR_DATA)).get(BLUE);}
	public float getGreen() { return ((Color) shapeData.get(COLOR_DATA)).get(GREEN); }
	public float getAlpha() { return ((Color) shapeData.get(COLOR_DATA)).get(ALPHA); }

	protected int vertexCount;

	// Engines can get/set data that they want to use by using this field
	protected Object additionalData;

	// will be multiplied to the MVP matrix before drawing the shape
	protected float[] mOwnMatrix = new float[16];

	public Object getData() { return ((Data) shapeData.get(ADDITIONAL_DATA)).get(); }

	public void setData(Object o)
	{
		shapeData.put(ADDITIONAL_DATA, new Data(o));
	}

	public float[] getMatrix()
	{
		return mOwnMatrix;
	}

	public void setMatrix(float[] matrix)
	{
		mOwnMatrix = matrix;
	}

	public Shape()
	{
		/*
			The code to compile/use shaders was moved to the
			initialize method below so that a method on a non-gl
			thread can create a shape and set its attributes
			and then call the generic "createShape" method that will
			properly initialize it in the renderer thread.
		*/
        shapeData = new ShapeData();
        setPos(new Position(0, 0, 0));
        setColor(new Color(0, 0, 0, 0));
        setData(null);
        setShaderToUse(null);
        Matrix.setIdentityM(mOwnMatrix, 0);
	}

	public boolean initialize(MainActivity mainActivity, String shaderName)
	{
        Managers managers = mainActivity.getManagers();

		Log.d("Shape", "Initializing shape with shader " + shaderName);

		//Check if a shader corresponding to this name was already initialized
		//If it is not the case, create it and initialize it, then attach it to this
		//shape.

		ShaderManager smgr = managers.getShaderManager();
		Shader shader;

		if (!smgr.isRegistered(shaderName))
		{
			if ((shader = smgr.create(mainActivity, shaderName)) != null) //create shader and register it
				smgr.register(shaderName, shader);
			else
			{
				Log.w("Shape", "Failed to get shader. Shape is not initialized and unusable");
				return false;
			}
		}
		else
		{
			if (DebugUtil.debugPrint(Constants.DEBUG_LEVEL_MORE_VERBOSE))
				Log.d("Shape", "Initialize - Shader was already initialized before ; using it");

            shader = smgr.get(shaderName);
		}

        managers.getShapeManager().registerShapeWithShader(this, shader);
        s = shader;

		int mProgram = s.getmProgram();
		mvpHandle = GLES20.glGetUniformLocation(mProgram, "u_MVPMatrix");
		mPositionHandle = GLES20.glGetAttribLocation(mProgram, "a_Position");
		mColorHandle = GLES20.glGetAttribLocation(mProgram, "a_Color");

		Matrix.setIdentityM(mOwnMatrix, 0);
		return true;
	}

	public void setShaderToUse(String shaderName)
	{
		this.shaderName = shaderName;
	}

	public Shader getShader()
	{
		return s;
	}

	public String getShaderToUse()
	{
		return shaderName;
	}

	protected abstract void refresh(List<Integer> indicesChanged);

	public abstract void draw(MainActivity mainActivity, float[] mvpMatrix);
}
