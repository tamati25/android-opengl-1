package com.tama.labo.shape;

import android.opengl.GLES20;

import com.tama.labo.attributes.Attribute;
import com.tama.labo.attributes.Color;
import com.tama.labo.attributes.Position;
import com.tama.labo.game.MainActivity;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;
import java.util.List;

public class Rectangle extends Shape
{
    private ShortBuffer indexBuffer;

    public static final int BOTTOM_LEFT_VERTEX = 0;
    public static final int BOTTOM_RIGHT_VERTEX = 1;
    public static final int TOP_RIGHT_VERTEX = 2;
    public static final int TOP_LEFT_VERTEX = 3;

	private float[] color = new float[16];

	public Rectangle()
	{
		super();
        init();
	}

    private void init()
    {
        for (int i = 0; i < 4; ++i)
        {
            color[4 * i] = 1.0f;
            color[4 * i + 1] = 0.0f;
            color[4 * i + 2] = 0.0f;
            color[4 * i + 3] = 1.0f;
        }
    }
    public void setVertexColor(int index, Color c) {
        color[4 * index] = c.get(Attribute.RED);
        color[4 * index + 1] = c.get(Attribute.GREEN);
        color[4 * index + 2] = c.get(Attribute.BLUE);
        color[4 * index + 3] = c.get(Attribute.ALPHA);
    }

    public Color getVertexColor(int index)
    {
        return new Color(color[4 * index], color[4 * index + 1], color[4 * index + 2], color[4 * index + 3]);
    }

    public float[] getVertexColor()
    {
        return color;
    }

    @Override
    public float getAlpha()
    {
        return (color[3] + color[7] + color[11] + color[15]) / 4.0f;
    }

    @Override
    public void setColor(Color c)
    {
        for (int i = 0; i < 4; ++i)
        {
            color[4 * i] = c.get(Attribute.RED);
            color[4 * i + 1] = c.get(Attribute.GREEN);
            color[4 * i + 2] = c.get(Attribute.BLUE);
            color[4 * i + 3] = c.get(Attribute.ALPHA);
        }
    }

	/* Implement inherited methods */
	@Override
	protected void refresh(List<Integer> indicesChanged) {
        Position p = getPos();
		float posx = p.get(Attribute.XPOS);
		float posy = p.get(Attribute.YPOS);
		float posz = p.get(Attribute.ZPOS);

		// The position given is the coordinates of the lower-left vertex
		// in the back.

		float[] coords =
			{
				//Front face
                posx, posy, posz,
                posx + size, posy, posz,
                posx + size, posy + size, posz,
                posx, posy + size, posz
			};

        short[] index = {0, 1, 2, 3, 0, 2};

		vertexBuffer = ByteBuffer.allocateDirect(coords.length * 4)
				.order(ByteOrder.nativeOrder()).asFloatBuffer();
		vertexBuffer.clear();
		vertexBuffer.put(coords);

        indexBuffer = ByteBuffer.allocateDirect(index.length * 2)
                .order(ByteOrder.nativeOrder()).asShortBuffer();
        indexBuffer.clear();
        indexBuffer.put(index);

		colorsBuffer = ByteBuffer.allocateDirect(color.length * 4)
				.order(ByteOrder.nativeOrder()).asFloatBuffer();
		colorsBuffer.clear();
		colorsBuffer.put(color);
	}

	@Override
	public void draw(MainActivity mainActivity, float[] mvpMatrix)
	{
        if (!shapeData.getDataChanged().isEmpty())
            refresh(shapeData.getDataChanged());

        vertexBuffer.position(0);
        indexBuffer.position(0);
        colorsBuffer.position(0);

        GLES20.glVertexAttribPointer(mPositionHandle, mPositionDataSize, GLES20.GL_FLOAT, false, 12, vertexBuffer);
        GLES20.glEnableVertexAttribArray(mPositionHandle);

        GLES20.glVertexAttribPointer(mColorHandle, mColorDataSize, GLES20.GL_FLOAT, false, 16, colorsBuffer);
        GLES20.glEnableVertexAttribArray(mColorHandle);

        GLES20.glUniformMatrix4fv(mvpHandle, 1, false, mvpMatrix, 0);
        GLES20.glDrawElements(GLES20.GL_TRIANGLES, 6, GLES20.GL_UNSIGNED_SHORT, indexBuffer);
    }
}
