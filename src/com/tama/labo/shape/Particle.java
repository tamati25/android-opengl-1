package com.tama.labo.shape;

import com.tama.labo.game.Game;


public abstract class Particle extends Shape
{
	// Particle class is not instantiable (abstract class)
	// Use one of its subclasses.
	
	public Particle() { super(); }
}
