package com.tama.labo.util;

import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.content.res.AssetManager;
import android.opengl.GLES20;
import android.util.Log;

import com.tama.labo.game.MainActivity;
import com.tama.labo.shader.Shader;


public class ShaderUtil
{
	public static int loadShader(Shader s, int type, String fileName, MainActivity mainActivity)
	{
	    // create a vertex shader type (GLES20.GL_VERTEX_SHADER)
	    // or a fragment shader type (GLES20.GL_FRAGMENT_SHADER)
	    int shader = GLES20.glCreateShader(type);

	    // add the source code to the shader and compile it

	    String shaderCode;

	    shaderCode = readFile(fileName, mainActivity);

	    Log.d("ShaderUtil", "Code read : " + shaderCode);

	    GLES20.glShaderSource(shader, shaderCode);
	    GLES20.glCompileShader(shader);

	    int[] compileStatus = new int[1];
	    GLES20.glGetShaderiv(shader, GLES20.GL_COMPILE_STATUS, compileStatus, 0);

	    Log.d("ShaderUtil", "Compile status : " + compileStatus[0]);

	    if (compileStatus[0] == 0)
	    {
	    	String error = GLES20.glGetShaderInfoLog(shader);

	    	GLES20.glDeleteShader(shader);
            //noinspection UnusedAssignment
            shader = 0;
	    Log.e("ShaderUtil.loadShader", "Failed to compile shader : " + error);
	    }

	    return shader;

	}

	private static String readFile(String fileName, MainActivity mainActivity)
	{
		/* Open the file from the assets folder */
		AssetManager assetMgr = mainActivity.getAssets();
		StringBuilder sb = new StringBuilder();

		try {
			InputStream is = assetMgr.open(fileName);
			byte[] data = new byte[1024];

			int count;

			while ((count = is.read(data)) > 0)
				sb.append(new String(data, 0, count));

			is.close();
		} catch (IOException e) {
			Log.w("ShaderUtil", "readFile - file not found in asset folder. Aborting.");
			return null;
		}

		return sb.toString();
	}
}
