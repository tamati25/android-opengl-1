package com.tama.labo.util;

public class Constants
{
	//Debug level constants
    public final static int DEBUG_LEVEL_QUIET = -1; //nothing will be printed
	public final static int DEBUG_LEVEL_ONLY_IMPORTANT = 0; //print only important log
	public final static int DEBUG_LEVEL_STANDARD = 1; //standard level
	public final static int DEBUG_LEVEL_VERBOSE = 2; //a little verbose, please ? :)
	public final static int DEBUG_LEVEL_MORE_VERBOSE = 3; //more verbose
	public final static int DEBUG_LEVEL_MOST_VERBOSE = 4; //all the information will be printed

	//Maximum number of particles on screen at a given time
	public final static int SHAPE_LIST_MAX_SIZE = 200;

	//Initial position of the eye
	public final static float RENDERER_INIT_EYE_X = 0.01f;
	public final static float RENDERER_INIT_EYE_Y = 0.01f;
	public final static float RENDERER_INIT_EYE_Z = 25;

	//How much can we zoom/de-zoom ?
	public final static float ZOOM_MIN = 0.2f;
	public final static float ZOOM_MAX = 2.5f;

    //Shader default name
    public final static String DEFAULT_SHADER_NAME = "BasicShader";
}
