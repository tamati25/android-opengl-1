package com.tama.labo.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.opengl.Matrix;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import com.tama.labo.attributes.Attribute;
import com.tama.labo.attributes.Coord;
import com.tama.labo.attributes.Position;
import com.tama.labo.game.MainActivity;

public class Util
{
	private static Display display = null;

	private static void initDisplay(Context ctx)
	{
		WindowManager wm = (WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE);
		display = wm.getDefaultDisplay();
	}

	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
    private static Point getScreenSize(Context ctx)
	{
		if (display == null)
			initDisplay(ctx);

		Point point = new Point();

		try
		{
			//API >= 13
			display.getSize(point);
		}
		catch (NoSuchMethodError e)
		{
			//API < 13
			point.x = display.getWidth();
			point.y = display.getHeight();
		}

		return point;
	}

	public static FloatBuffer ArrayListToFloatBuffer(ArrayList<Float> list)
	{
		int size = list.size();

		FloatBuffer result = ByteBuffer.allocateDirect(size * 4)
				.order(ByteOrder.nativeOrder())
				.asFloatBuffer();
		result.clear();

        for (Float aList : list) {
            result.put(aList);
        }

		return result;
	}

	public static ShortBuffer ArrayListToShortBuffer(ArrayList<Short> list)
	{
		int size = list.size();

		ShortBuffer result = ByteBuffer.allocateDirect(size * 2)
				.order(ByteOrder.nativeOrder())
				.asShortBuffer();
		result.clear();

        for (Short aList : list) {
            result.put(aList);
        }

		return result;
	}

	public static String getRawData(final InputStream is)
	{
		final char[] buffer = new char[1024];
		final StringBuilder out = new StringBuilder();
		try
		{
			final Reader in = new InputStreamReader(is, "UTF-8");
			for (;;) {
				int rsz = in.read(buffer, 0, buffer.length);
				if (rsz < 0) break;
				out.append(buffer, 0, rsz);
			}
			in.close();
		}
		catch (UnsupportedEncodingException e) {
			Log.w("ObjParser", "UnsupportedEncodingException : " + e.getMessage());
			return null;
		} catch (IOException e) {
			Log.w("ObjParser", "IOException : " + e.getMessage());
			return null;
		}

		return out.toString();
	}

	/**
	 * Calculates the transform from screen coordinate system to world coordinate system coordinates
	 * for a specific point, given a camera position.
	 *
         * from http://stackoverflow.com/questions/10985487/android-opengl-es-2-0-screen-coordinates-to-world-coordinates
	 *
	 * @param touch point of screen touch, the actual position on physical screen (ej: 160, 240)
     * @return position in WCS.
     */
	public static Position GetWorldCoords(MainActivity mainActivity, Coord touch)
	{
       // SCREEN height & width (ej: 320 x 480)
	   Point screen = Util.getScreenSize(mainActivity);
       float screenW = screen.x;
       float screenH = screen.y;

       // Auxiliary matrix and vectors to deal with ogl
       float[] invertedMatrix, transformMatrix, normalizedInPoint, outPoint;
	   invertedMatrix = new float[16];
	   transformMatrix = new float[16];
	   normalizedInPoint = new float[4];
	   outPoint = new float[4];

	   // Invert y coordinate, as android uses top-left, and ogl bottom-left.
	   int oglTouchY = (int) (screenH - touch.get(Attribute.XPOS));

	   // Transform the screen point to clip space in ogl (-1,1)
	   normalizedInPoint[0] = (float) ((touch.get(Attribute.XPOS)) * 2.0f / screenW - 1.0);
	   normalizedInPoint[1] = (float) ((oglTouchY) * 2.0f / screenH - 1.0);
	   normalizedInPoint[2] = - 1.0f;
	   normalizedInPoint[3] = 1.0f;

	   // Obtain the transform matrix and then the inverse
       float[] modelMatrix = new float[16];
       Matrix.setIdentityM(modelMatrix, 0);

       float[] vpMatrix = mainActivity.getWorld().getVPMatrix();

       //TODO : Check it ?
       Matrix.multiplyMM(transformMatrix, 0, vpMatrix, 0, modelMatrix, 0);
       Matrix.invertM(invertedMatrix, 0, transformMatrix, 0);

       //Apply the inverse to the point in clip space
       Matrix.multiplyMV(outPoint, 0, invertedMatrix, 0, normalizedInPoint, 0);

	   if (outPoint[3] == 0.0)
	   {
	       // Avoid /0 error.
	       Log.e("World coords", "ERROR!");
	       return null;
	   }

	   // Divide by the 3rd component to find out the real position.
	   return new Position(outPoint[0] / outPoint[3], outPoint[1] / outPoint[3], outPoint[2] / outPoint[3]);
	}

	/*
	 * Test if a class is a subclass of another
	 * Example of call : Util.isSubclassof(Car.class, Vehicle.class)
	 */
	public static boolean isSubclassOf(Class<?> tested, Class<?> tester)
	{
		return (tested.getSuperclass().equals(tester));
	}

	// Load a height map from a bitmap file
	// Use darker colors for low places, and lighter for high places
	public static float[] loadHeightMap(String path, int width, int height, int minHeight, int maxHeight)
	{
		float[] result = null;
		File f = new File(path);
		try {
			InputStream is = new FileInputStream(f);
			result = loadHeightMap(is, width, height, minHeight, maxHeight);
			is.close();
		} catch (FileNotFoundException e) {
			Log.w("LoadHeightMap", "Couldn't open file " + path + ": FileNotFoundException");
			e.printStackTrace();
		} catch (IOException e) {
			Log.w("LoadHeightMap", "Error opening file " + path + ": IOException");
			e.printStackTrace();
		}

		return result;
	}

	// Load a height map from a bitmap in the assets folder
	public static float[] loadHeightMap(Context c, String path, int width, int height, int minHeight, int maxHeight)
	{
		float[] result = null;
		try {
			InputStream is = c.getAssets().open(path);
			result = loadHeightMap(is, width, height, minHeight, maxHeight);
			is.close();
		} catch (IOException e) {
			Log.w("LoadHeightMap", "Error opening file " + path + ": IOException");
			e.printStackTrace();
		}

		return result;
	}

	//Main function for loading height map from bitmap
	public static float[] loadHeightMap(InputStream is, int width, int height, int minHeight, int maxHeight)
	{
		float[] result = new float[width * height];

		Bitmap out = getHeightBitmap(is, width, height);

		if (out == null)
		{
			Log.w("LoadHeightMap", "Couldn't get bitmap, returning.");
			return result;
		}

		//Copy image data
		int[] raw = new int[width * height];
		out.getPixels(raw, 0, width, 0, 0, width, height);

		final int diffHeight = maxHeight - minHeight;

		for (int row = 0 ; row < height; ++row)
		{
			for (int col = 0; col < width; ++col)
			{
				int indice = row * width + col;
				int total = Color.red(raw[indice]) + Color.green(raw[indice]) + Color.blue(raw[indice]);

				//The maximum value possible for total is 255 * 3 = 765
				result[indice] = (float) (minHeight + ((float)total / 765.0) * diffHeight);
			}
		}

		return result;
	}

	// Load a bitmap, checking if dimensions are appropriate
	// See http://developer.android.com/training/displaying-bitmaps/load-bitmap.html
	private static Bitmap getHeightBitmap(InputStream input, int width, int height)
	{
		//FIXME : What if the user want to load a large image ? (OutOfMemoryException ...)
		Bitmap out = BitmapFactory.decodeStream(input);

		if (out == null)
		{
			Log.w("getHeightBitmap", "Couldn't decode bitmap (1), returning.");
			return null;
		}

		final int bitmapHeight = out.getHeight();
		final int bitmapWidth = out.getWidth();

		if (bitmapHeight < height || bitmapWidth < width)
		{
			Log.w("getHeightBitmap", "Invalid size : bitmap is " + bitmapHeight + " x " + bitmapWidth);
			Log.w("getHeightBitmap", "Minimum size is " + width + " x " + height);
			return null;
		}

		return out;
	}
}
