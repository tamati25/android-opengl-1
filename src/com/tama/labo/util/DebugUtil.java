package com.tama.labo.util;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.util.Log;

import com.tama.labo.shape.Shape;

public class DebugUtil
{
	//set to true if in debug mode (display more information)
	public static boolean isDebugMode = true;

    @SuppressLint("UseSparseArrays")
	private static final HashMap<Integer, Integer> debugList = new HashMap<Integer, Integer>();
	private static final HashMap<Object, Integer> debugListObj = new HashMap<Object, Integer>();
	private static int nextId = Integer.MIN_VALUE;

	public static boolean shouldRun()
	{
        return false;
	}

	public static boolean debugPrint(int infoLvl)
	{
        int debugVerboseLevel = Constants.DEBUG_LEVEL_STANDARD;
        return ((isDebugMode) && (debugVerboseLevel >= infoLvl));
	}

	/*
	 * Log utilities
	 */

	// Only display log if the debug level is high enough
	public static void logD(String tag, String msg, int infoLvl)
	{
		if (!debugPrint(infoLvl))	return;

		Log.d(tag, msg);
	}

	public static void logW(String tag, String msg, int infoLvl)
	{
		if (!debugPrint(infoLvl))	return;

		Log.w(tag, msg);
	}

	/*
	 * Dump methods
	 */
	public static String dumpFloatBuffer(FloatBuffer buffer)
	{
		StringBuilder result = new StringBuilder();
		result.append("Current position / capacity : ");
		result.append(buffer.position()).append(" / ").append(buffer.capacity()).append("\n");

		result.append("--- Start dump ---\n");

		for (int i = 0; i < buffer.capacity(); ++i)
		{
			result.append(i).append(" : ").append(String.valueOf(buffer.get(i))).append("\n");
		}
		result.append("--- End dump ---\n");

		return result.toString();
	}

	public static String dumpShortBuffer(ShortBuffer buffer)
	{
		StringBuilder result = new StringBuilder();
		result.append("Current position / capacity : ");
		result.append(buffer.position()).append(" / ").append(buffer.capacity()).append("\n");

		result.append("--- Start dump ---\n");

		for (int i = 0; i < buffer.capacity(); ++i)
		{
			result.append(i).append(" : ").append(String.valueOf(buffer.get(i))).append("\n");
		}
		result.append("--- End dump ---\n");

		return result.toString();
	}

	public static String dumpFloatArray(float[] list)
	{
		return Arrays.toString(list);
	}

	public static String dumpShortArray(short[] list)
	{
		return Arrays.toString(list);
	}

	/*
	 * "Limited debug" methods
	 */
	public static int getNextAvailableId()
	{
		return nextId;
	}

	/*
	 * Will print debug information only once (useful when debugging in loops to avoid
	 * having too much log)
	 * Default level when infoLvl is omitted is DEBUG_LEVEL_STANDARD (1)
	 */
	public static void logOnce(String tag, String text, int id)
	{
		logNTimes(tag, text, Constants.DEBUG_LEVEL_STANDARD, id, 1);
	}

	public static void logOnce(String tag, String text, int infoLvl, int id)
	{
		logNTimes(tag, text, infoLvl, id, 1);
	}

	/*
	 * Will display the information exactly N times in the log
	 * Default level of logging is DEBUG_LEVEL_STANDARD (1)
	 */
	public static void logNTimes(String tag, String text, int id, int count)
	{
		logNTimes(tag, text, Constants.DEBUG_LEVEL_STANDARD, id, count);
	}

	public static void logNTimes(String tag, String text, int infoLvl, int id, int count)
	{
		if (id >= nextId)
			nextId = id + 1;

		if (!debugPrint(infoLvl))
			return; //You're not important enough for me. Go away.

		int remaining = count;

		if (debugList.containsKey(id))
		{
			//We have already displayed this log at least once, check if we should display it
			//again (remaining > 0)

			remaining = debugList.get(id);

			if (remaining == 0) return;
		}

		//display log
		Log.d(tag, text);

		//decrease count by 1
		debugList.put(id, remaining - 1);
	}

	/*
	 * Will display the information exactly once per object (useful when a class
	 * can have multiple instances)
	 * Default level of logging is DEBUG_LEVEL_STANDARD (1)
	 */
	public static void logOncePerInstance(String tag, String text, Object instance)
	{
		logNTimesPerInstance(tag, text, instance, Constants.DEBUG_LEVEL_STANDARD, 1);
	}

	public static void logOncePerInstance(String tag, String text, Object instance, int infoLvl)
	{
		logNTimesPerInstance(tag, text, instance, infoLvl, 1);
	}

	/*
	 * Will display the information exactly N times per object (useful when a class
	 * can have multiple instances)
	 * Default level of logging is DEBUG_LEVEL_STANDARD (1)
	 */
	public static void logNTimesPerInstance(String tag, String text, Object instance, int count)
	{
		logNTimesPerInstance(tag, text, instance, Constants.DEBUG_LEVEL_STANDARD, count);
	}

	public static void logNTimesPerInstance(String tag, String text, Object instance, int infoLvl, int count)
	{
		if (!debugPrint(infoLvl))
			return; //You're not important enough for me. Go away.

		int remaining = count;

		if (debugListObj.containsKey(instance))
		{
			//We have already displayed this log at least once, check if we should display it
			//again (remaining > 0)

			remaining = debugListObj.get(instance);

			if (remaining == 0) return;
		}

		//display log
		Log.d(tag, text);

		//decrease count by 1
		debugListObj.put(instance, remaining - 1);
	}

	public static void dumpShapeArrayList(ArrayList<Shape> list)
	{
		for (Shape s : list)
		{
			Log.d("DebugUtil", s.toString());
		}

	}
}
