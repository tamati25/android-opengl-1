package com.tama.labo.util;

import java.util.Random;

public class MathUtil
{
	//public static final Random r = new Random();
	public static final Random r = new Random(42); //benchmark test with same seed

	public static double deg2rad(double deg)
	{
		 return deg * Math.PI / 180;
	}

	/*
	 * Compute the normal vector of a polygon, given its vertices coordinates (x, y, z)
	 * from http://www.opengl.org/wiki/Calculating_a_Surface_Normal#Newell.27s_Method
	 *
	 * Input :
	 * 		float[] vertices : vertices in (x, y, z) format
	 * Output : normal vector
	 * 		float[] normal : normal vector (nx, ny, nz)
	 */
	public static float[] getNormal(float[] vertices)
	{
		int numberOfVertices = vertices.length / 3;

		float[] normal = {0, 0, 0};

		for (int i = 0; i < numberOfVertices; ++i)
		{
			int nextVertexIndex = (i + 1) % numberOfVertices;

			float[] currentVertex = {vertices[3 * i], vertices[3 * i + 1], vertices[3 * i + 2]};
			float[] nextVertex = {vertices[3 * nextVertexIndex],
								   vertices[3 * nextVertexIndex + 1],
								   vertices[3 * nextVertexIndex + 2]};

			normal[0] = normal[0] + (currentVertex[1] - nextVertex[1]) * (currentVertex[2] - nextVertex[2]);
			normal[1] = normal[1] + (currentVertex[2] - nextVertex[2]) * (currentVertex[0] - nextVertex[0]);
			normal[2] = normal[2] + (currentVertex[0] - nextVertex[0]) * (currentVertex[1] - nextVertex[1]);
		}

		return normal;
	}

	/*
	 * Compute the curve at the given row / called by CurvedWall.buildWall
	 */
	public static double getCurve(int j, float max)
	{
		return getCurve(j, max, 50);
	}

	public static double getCurve(int j, float max, int middle)
	{
		return (max/middle * Math.sqrt(2 * middle - j) * Math.sqrt(j));
	}

	public static float smoothStep(float p)
	{
		//p is between 0 and 1
		return p * p * (3 - 2 * p);
	}

	public static float getSquareDistanceBetween(float x1, float y1, float x2, float y2)
	{
		return ((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
	}

	public static float keepBetween(float min, float f, float max)
	{
		//keep f between min and max
		return Math.min(Math.max(f, min), max);
	}
}
