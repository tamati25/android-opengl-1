package com.tama.labo.world;

import android.opengl.Matrix;

import com.tama.labo.attributes.Attribute;
import com.tama.labo.attributes.Position;

import org.w3c.dom.Attr;

/**
 * Created by tama on 23/07/14.
 */
public class Camera
{
    private float[] viewMatrix = new float[16];
    private float[] rotMatrix = new float[16];

    private Position cameraPos = new Position();
    private Position lookAt = new Position();
    private Position up = new Position(0.0f, 1.0f, 0.0f);

    private float zoom = 1.0f;

    /*
        Constructors
     */
    public Camera()
    {
        Matrix.setIdentityM(viewMatrix, 0);
        Matrix.setIdentityM(rotMatrix, 0);
        zoom = 1.0f;
    }

    /*
        Methods to move camera
     */

    /* --- Rotation --- */
    public Camera resetRotation()
    {
        Matrix.setIdentityM(rotMatrix, 0);
        return this;
    }

    public Camera rotateCameraX(float angle)
    {
        Matrix.rotateM(rotMatrix, 0, angle, 1.0f, 0.0f, 0.0f);
        return this;
    }

    public Camera rotateCameraY(float angle)
    {
        Matrix.rotateM(rotMatrix, 0, angle, 0.0f, 1.0f, 0.0f);
        return this;
    }

    public Camera rotateCameraZ(float angle)
    {
        Matrix.rotateM(rotMatrix, 0, angle, 0.0f, 0.0f, 1.0f);
        return this;
    }

    /* --- Scale ("zoom") --- */
    public Camera zoom(float scale)
    {
        zoom = zoom * scale;
        return this;
    }

    /* --- Translate ("move") --- */
    public Camera moveX(float value)
    {
        cameraPos.set(Attribute.XPOS, cameraPos.get(Attribute.XPOS) + value);
        return this;
    }

    public Camera moveY(float value)
    {
        cameraPos.set(Attribute.YPOS, cameraPos.get(Attribute.YPOS) + value);
        return this;
    }

    public Camera moveZ(float value)
    {
        cameraPos.set(Attribute.ZPOS, cameraPos.get(Attribute.ZPOS) + value);
        return this;
    }

    public Camera moveTo(float x, float y, float z)
    {
        cameraPos = new Position(x, y, z);
        return this;
    }

    public Camera lookAt(float x, float y, float z)
    {
        lookAt = new Position(x, y, z);
        return this;
    }

    /*
        Get camera matrix (view matrix)
     */
    public float[] getViewMatrix()
    {
       Matrix.setIdentityM(viewMatrix, 0);
       Matrix.setLookAtM(viewMatrix, 0,
                        cameraPos.get(Attribute.XPOS), cameraPos.get(Attribute.YPOS), cameraPos.get(Attribute.ZPOS),
                        lookAt.get(Attribute.XPOS), lookAt.get(Attribute.YPOS), lookAt.get(Attribute.ZPOS),
                        up.get(Attribute.XPOS), up.get(Attribute.YPOS), up.get(Attribute.ZPOS));
       Matrix.scaleM(viewMatrix, 0, zoom, zoom, zoom);
       Matrix.multiplyMM(viewMatrix, 0, viewMatrix, 0, rotMatrix, 0);
       return viewMatrix;
    }
}
