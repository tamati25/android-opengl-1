package com.tama.labo.world;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;

import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.Log;

import com.tama.labo.engine.Engine;
import com.tama.labo.game.MainActivity;
import com.tama.labo.interfaces.IMap;
import com.tama.labo.interfaces.IMethod;
import com.tama.labo.manager.Managers;
import com.tama.labo.manager.ShapeManager;
import com.tama.labo.shader.Shader;
import com.tama.labo.shape.Shape;
import com.tama.labo.util.Constants;
import com.tama.labo.util.DebugUtil;

/**
 * Created by tama on 23/07/14.
 */
public class World
{
    private float[] projMatrix = new float[16];
    private Camera camera;

    private Managers managers;

    public World(Managers managers) {
		this.managers = managers;
        camera = new Camera();
    }

    public World setCamera(Camera camera)
    {
        this.camera = camera;
        return this;
    }

    public World setProjMatrix(float[] projMatrix)
    {
        this.projMatrix = projMatrix;
        return this;
    }

    public void clear()
    {
        synchronized(managers.getShapeManager().getAll())
        {
            managers.getShapeManager().clear();
        }
    }

    /* -- getters -- */
    public Camera getCamera()
    {
        return this.camera;
    }

    public float[] getVPMatrix()
    {
        float[] result = new float[16];

        Matrix.setIdentityM(result, 0);

        Matrix.multiplyMM(result, 0, projMatrix, 0, camera.getViewMatrix(), 0);

        return result;
    }

    /* Render world */
    public void render(List<Shape> shape)
    {
        if (shape == null)
        {
            //Render all shapes
            managers.getShapeManager().applyAll(new IMap<Shape>() {
                @Override
                public void apply(Shape obj) {
                    if (DebugUtil.debugPrint(Constants.DEBUG_LEVEL_VERBOSE))
                        Log.d("MyRenderer", "\t" + obj.toString());

                    float[] mvpMatrix = new float[16];
                    Matrix.setIdentityM(mvpMatrix, 0);

                    //Multiply with the view matrix
                    Matrix.multiplyMM(mvpMatrix, 0, getVPMatrix(), 0, mvpMatrix, 0);

                    //Multiply with the model matrix (default = identity)
                    Matrix.multiplyMM(mvpMatrix, 0, obj.getMatrix(), 0, mvpMatrix, 0);

			    /* Draw shapes */
                    obj.draw(managers.getMainActivity(), mvpMatrix);
                }
            }, new IMethod<Shader>() {
                @Override
                public void run(Shader shader) {
                    GLES20.glUseProgram(shader.getmProgram());
                }
            });
        }
        else
        {
            for (Shape s : shape)
            {
                float[] mvpMatrix = new float[16];
                Matrix.setIdentityM(mvpMatrix, 0);

                //Multiply with the view matrix
                Matrix.multiplyMM(mvpMatrix, 0, getVPMatrix(), 0, mvpMatrix, 0);

                //Multiply with the model matrix (default = identity)
                Matrix.multiplyMM(mvpMatrix, 0, s.getMatrix(), 0, mvpMatrix, 0);

			    /* Draw shapes */
                GLES20.glUseProgram(s.getShader().getmProgram());
                s.draw(managers.getMainActivity(), mvpMatrix);
            }
        }

        managers.getMainActivity().getView().requestRender();
    }
}
