package com.tama.labo.engine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map.Entry;
import java.util.Set;

import android.opengl.Matrix;
import android.util.Log;
import android.view.MotionEvent;

import com.tama.labo.attributes.Position;
import com.tama.labo.game.Game;
import com.tama.labo.interfaces.IMap;
import com.tama.labo.manager.Managers;
import com.tama.labo.shape.BasicParticle;
import com.tama.labo.shape.MyShape;
import com.tama.labo.shape.Shape;
import com.tama.labo.util.Constants;
import com.tama.labo.util.MathUtil;
import com.tama.labo.world.Camera;

public class Engine2 extends Engine
{
    private int angle = 0;

	//private boolean canMerge = true;

	//private int particleCount = 0;

	private Collection<Shape> toBeRemoved = new ArrayList<Shape>();
	private Collection<Shape> toBeAdded = new ArrayList<Shape>();

    public static class Delta
	{
		public float dx, dy, dz;
		public String moveDir;
	}

    public Engine2(Managers managers)
    {
    	super(managers);
    }

	@Override
	public void init(Game game)
	{
		//canMerge = false;
        BasicParticle bp = new BasicParticle();
        bp.setPos(new Position(0f, 1.0f, 0f));

        managers.getShapeManager().create(new MyShape());
        managers.getShapeManager().create(bp);

        Camera camera = new Camera();
        camera.moveTo(0.01f, 5.0f, 0.01f);
        camera.lookAt(0f, 0f, 0f);
        game.getWorld().setCamera(camera);
    }

	public void run() {

		while (isRunning != null && isRunning)
		{
			synchronized(managers.getShapeManager().getAll())
			{
                managers.getShapeManager().applyAll(new IMap<Shape>() {
                    @Override
                    public void apply(Shape s) {
                        float[] ownMatrix = new float[16];
                        Matrix.setIdentityM(ownMatrix, 0);

                        if (s instanceof BasicParticle) {
                            BasicParticle p = (BasicParticle) s;
                            if (handleBasicParticle(p))
                                managers.getShapeManager().destroyShape(p);
                        } else if (s instanceof MyShape) {
                            //rotate around its own y axis
                            handleMyShape(ownMatrix);
                        }

                        s.setMatrix(ownMatrix);

                    }
                }, null, null);
			}

			//Reset the list of shapes that are marked to be removed
			toBeRemoved.clear();
			toBeAdded.clear();

			try {
                long sleepTime = 50L;
                Thread.sleep(sleepTime); } catch (InterruptedException ignored) {}
		}
	}

	//private void handleParticleCollision()
	//{
		/*
		 * Handle collisions between particles
		 *
		 * Collisions rules :
		 *
		 *   1. A particle will collide with another one if the distance between their respective centers is
		 *      inferior or equal to the maximum of (SizeOfParticleA / 4, SizeOfParticleB / 4)
		 *
		 *   2. When 2 particles collide, they will merge into one with the following attributes :
		 *          - Position : at the center of the particles
		 *          - Size : Average of the size of the two particles
		 *          - Color : Average of the colors of the particles (i.e black + white = gray)
		 *          - Data : If the data of one of the particles is null, choose randomly between xy and xz, else
		 *                   use the following table :
		 *              +--------+--------+--------+
		 *              |       A|        |        |
		 *              |        |   xy   |   xz   |
		 *              |B       |        |        |
		 *              +--------+--------+--------+
		 *              |        |        |  50% xy|
		 *              |   xy   | 100% xy|        |
		 *              |        |        |  50% xz|
		 *              +--------+--------+--------+
		 *              |        |  50% xy|        |
		 *              |   xz   |        | 100% xz|
		 *              |        |  50% xz|        |
		 *              +--------+--------+--------+
		 *
		 *   3. Particles may merge only once per "turn" : if particles A and B merge into particle AB
		 *      and follow the rule (1) with a nearby particle C, they will _not_ merge again
		 */

        //TODO
	//}

	private Delta setMovement(Delta a, Delta b)
	{
		Delta result = new Delta();

		float angle = MathUtil.r.nextInt(180);
		float v0 = MathUtil.r.nextInt(3);
		result.dx = (float)(v0 * Math.cos(MathUtil.deg2rad(angle)));

		// Prepare a random number which is 0 or 1
		int randomNumber = MathUtil.r.nextInt(2);

		// Determine movement direction
		if (a == null || b == null)
		{
			result.moveDir = (randomNumber == 0 ? "xy" : "xz" );
		}
		else
		{
			if (a.equals(b))
				result.moveDir = a.moveDir;
			else
				result.moveDir = (randomNumber == 0 ? a.moveDir : b.moveDir);
		}

		if (result.moveDir.equals("xy"))
		{
			result.dy = (float)(v0 * Math.sin(MathUtil.deg2rad(angle)));
			result.dz = 0.1f;
		}
		else
		{
			result.dy = 0.1f;
			result.dz = (float)(v0 * Math.sin(MathUtil.deg2rad(angle)));
		}
		return result;
	}

	/* Engine methods */
	private boolean handleBasicParticle(BasicParticle p)
	{
		if (toBeRemoved.contains(p))    return false; //nothing to do here,

		Delta data = (Delta)p.getData();

		if (data == null)
		{
			p.setData(setMovement(null, null));
		}
		else
		{
			//move
			p.setPosX(p.getPosX() + data.dx);
			p.setPosY(p.getPosY() + data.dy);
			p.setPosZ(p.getPosZ() + data.dz);

			if (data.moveDir.equals("xy"))
				data.dy -= 0.1f;
			else
				data.dz -= 0.1f;
		}

		if (p.getAlpha() > 0.05f)
		{
			p.setAlpha(p.getAlpha() * 0.9f);
			p.setData(data);
		}
		else
		{
			//no longer want this particle (alpha too low ; about 300 iterations)
			managers.getShapeManager().destroyShape(p);
			toBeRemoved.add(p);
			//particleCount--;
			return true;
		}

		return false;
	}

	private void handleMyShape(float[] ownMatrix)
	{
        //operations must be placed in inverse order :
        Matrix.translateM(ownMatrix, 0, 0, -5f, 0); //3. translate back
        Matrix.rotateM(ownMatrix, 0, angle, 0, 1, 0); //2. rotate
        Matrix.translateM(ownMatrix, 0, 0, 5f, 0); //1. translate to origin
        angle += 5;
	}

	@Override
	public boolean onTouch(Game game, MotionEvent e) {
		switch (e.getAction())
		{
			case MotionEvent.ACTION_DOWN:
			{
                boolean addParticleOnTouch = true;
                if (addParticleOnTouch)
				{
					if (managers.getShapeManager().getShapeCount(BasicParticle.class) < Constants.SHAPE_LIST_MAX_SIZE)
					{
						Log.d("MySurfaceView", "Touch detected, adding particle.");
						BasicParticle basicParticle = new BasicParticle();
						basicParticle.setPos(new Position(0, 0, 0));
                        managers.getShapeManager().create(basicParticle);
					}
					else
						Log.w("MySurfaceView", "Touch detected, but no space to create particle.");
				}
				return true;
			}
		}
		return false;
	}

	public static float epsilon()
	{
		//return a random float between -0.1 and 0.1
		float r = MathUtil.r.nextFloat() / 10;

		return (MathUtil.r.nextFloat() < 0.5 ? r : -r);
	}
}
