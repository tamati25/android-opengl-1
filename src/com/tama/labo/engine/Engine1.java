package com.tama.labo.engine;

import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.Set;

import android.opengl.Matrix;
import android.util.Log;

import com.tama.labo.attributes.Position;
import com.tama.labo.game.Game;
import com.tama.labo.interfaces.IMap;
import com.tama.labo.manager.Managers;
import com.tama.labo.shape.BasicParticle;
import com.tama.labo.shape.Particle;
import com.tama.labo.shape.Shape;

public class Engine1 extends Engine
{
	public Engine1(Managers managers)
	{
		super(managers);
	}

    public void init(Game game)
    {
    }

    public void run()
	{
		while (isRunning != null && isRunning)
		{
			synchronized(managers.getShapeManager().getAll())
			{
                managers.getShapeManager().applyAll(new IMap<Shape>() {
                    @Override
                    public void apply(Shape s) {
                        if (s instanceof BasicParticle) {
                            BasicParticle p = (BasicParticle) s;

                            float lastPos = p.getPosX();

                            p.setPosX(p.getPosX() + 0.05f);
                            p.setAlpha(p.getAlpha() * 0.99f);

                            if ((lastPos < 3) && (p.getPosX() >= 3)) {
                                Particle newParticle = new BasicParticle();
                                newParticle.setPos(new Position(0, 0, 0));
                                managers.getShapeManager().create(newParticle);
                            }

	    					/* No special transformation, keep the identity matrix */
                            float[] identity = new float[16];
                            Matrix.setIdentityM(identity, 0);
                            p.setMatrix(identity);
                        }
                    }
                }, null, null);
			}

			try {
                long sleepTime = 50L;
                Thread.sleep(sleepTime); } catch (InterruptedException ignored) {}
		}
	}

    @Override
	public void finish() {
		Log.d("OpenGL", "isRunning = false, end of loop");
	}
}
