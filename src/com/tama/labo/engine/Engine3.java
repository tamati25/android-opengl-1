package com.tama.labo.engine;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.opengl.GLES20;
import android.opengl.Matrix;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import com.tama.labo.attributes.Attribute;
import com.tama.labo.attributes.Position;
import com.tama.labo.game.Game;
import com.tama.labo.interfaces.IMap;
import com.tama.labo.manager.Managers;
import com.tama.labo.shape.Shape;
import com.tama.labo.shape.TexturedParticle;
import com.tama.labo.util.Constants;
import com.tama.labo.util.DebugUtil;
import com.tama.labo.world.Camera;

public class Engine3 extends Engine
{
	//Engine created to test textured particles
	//Make them spin on themselves around X and Y
	//axis

	private int angle = 0;

    private float worldRotateAngle = 0;
	private float tmpWorldRotateAngle = 0;

	private float initX = -1;

    private static final float dxForOneTurn = 100.0f;

    private int effect = 0;

    public Engine3(Managers managers)
    {
    	super(managers);
        //We want to use a custom layout !
        useCustomLayout = true;
        mCustomLayout = R.layout.engine3_layout;
        orientation = ScreenOrientation.SCREEN_ORIENTATION_BOTH;
    }

    private void initButtons()
    {
        initButton(R.id.no_effect_btn, new View.OnClickListener() {
            public void onClick(View v) {
                DebugUtil.logD("Engine3", "Set effect to 0", Constants.DEBUG_LEVEL_STANDARD);
                effect = 0;
            }
        });

        initButton(R.id.bw_effect_btn, new View.OnClickListener() {
            public void onClick(View v) {
                DebugUtil.logD("Engine3", "Set effect to 1", Constants.DEBUG_LEVEL_STANDARD);
                effect = 1;
            }
        });

        initButton(R.id.fog_effect_btn, new View.OnClickListener() {
            public void onClick(View v) {
                DebugUtil.logD("Engine3", "Set effect to 2", Constants.DEBUG_LEVEL_STANDARD);
                effect = 2;
            }
        });
    }

    private void initButton(final int buttonId, final View.OnClickListener listener)
    {
        managers.runOnUIThread(new Runnable() {
            public void run() {
                Button button = (Button) managers.getMainActivity().findViewById(buttonId);
                if (button != null) {
                    button.setOnClickListener(listener);
                } else {
                    DebugUtil.logD("Engine3", "Button not found", Constants.DEBUG_LEVEL_STANDARD);
                }
            }
        });
    }

	@Override
	public void setGLParameters(GL10 p1, EGLConfig p2)
	{
		GLES20.glEnable(GLES20.GL_DEPTH_TEST);

		//Set the blending function, see http://i.stack.imgur.com/i2IAC.jpg for chart
		//(i.e http://gamedev.stackexchange.com/questions/29492/opengl-blending-help-needed)
		GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
	}

	@Override
	public void init(Game game)
	{
		TexturedParticle tp1 = new TexturedParticle();
		tp1.setPos(new Position(1, 0, 0));
        tp1.setSize(2.0f);
		tp1.setUniformTexture(managers.getTextureManager().get("Android"));
        tp1.setShaderToUse("TextureShader");
		tp1.setData("Horz");

        TexturedParticle tp2 = new TexturedParticle();
        tp2.setPos(new Position(-3, 0, 0));
        tp2.setSize(2.0f);
        tp2.setUniformTexture(managers.getTextureManager().get("Chuuni"));
        tp2.setShaderToUse("TextureShader");
        tp2.setData("Vert");

        //Set the world camera
        Camera camera = new Camera();
        camera.moveTo(Constants.RENDERER_INIT_EYE_X, Constants.RENDERER_INIT_EYE_Y, Constants.RENDERER_INIT_EYE_Z);
        camera.lookAt(0.0f, 0.0f, 0.0f);
        game.getWorld().setCamera(camera);

        managers.getShapeManager().create(tp1);
        managers.getShapeManager().create(tp2);

        initButtons();
	}

	public void run()
	{
        while (isRunning != null && isRunning)
		{
			synchronized(managers.getShapeManager().getAll())
			{
                managers.getShapeManager().applyAll(new IMap<Shape>() {
                    @Override
                    public void apply(Shape s) {
                        float[] matrix = new float[16];
                        Matrix.setIdentityM(matrix, 0);

                        // We should only have 2 textured particles on screen, but we
                        // test to be sure
                        if (s instanceof TexturedParticle) {
                            TexturedParticle tp = (TexturedParticle) s;

                            String rotateAxis = (String) tp.getData();
                            Position pos = tp.getPos();

                            //Reminder : the first operation to be executed is the last one in
                            //the code
                            float xcenter = pos.get(Attribute.XPOS) + tp.getSize() / 2;
                            float ycenter = pos.get(Attribute.YPOS) + tp.getSize() / 2;
                            float zcenter = pos.get(Attribute.ZPOS) + tp.getSize() / 2;

                            //3. translate back
                            Matrix.translateM(matrix, 0, xcenter, ycenter, zcenter);

                            //2. rotate around the correct axis
                            if (rotateAxis.equals("Horz")) {
                                //horizontal => around Y axis
                                Matrix.rotateM(matrix, 0, angle, 0, 1, 0);
                            } else {
                                //vertical => around X axis
                                Matrix.rotateM(matrix, 0, angle, 1, 0, 0);
                                //Un-comment this line for fade effect :)
                                //tp.setAlpha(tp.getAlpha() * 0.98f);
                            }

                            //1. translate so the center is at (0, 0, 0)
                            Matrix.translateM(matrix, 0, -xcenter, -ycenter, -zcenter);
                        }

                        //Set matrix
                        s.setMatrix(matrix);
                    }
                }, null, null);
			}
			angle += 10;
			try {
                long sleepTime = 25L;
                Thread.sleep(sleepTime); } catch (Exception ignored) {}
		}
	}

	@Override
	public boolean onTouch(Game game, MotionEvent e)
	{
		float eventX = e.getX();
		int action = e.getActionMasked();

		boolean out = false; //don't consume event

		switch(action)
		{
			case MotionEvent.ACTION_DOWN:
			  initX = eventX;
			  break;

			case MotionEvent.ACTION_UP:
			  worldRotateAngle = tmpWorldRotateAngle;
			  break;
		}
		return out;
	}

	@Override
	public boolean onTouchAfter(Game game, MotionEvent e)
	{
		//We test "move" event after the zoom event, to avoid zooming and moving at the
		//same time (the zoom event will be consumed in the surface view)
		int action = e.getActionMasked();
		float eventX = e.getX();

		float[] rot = new float[16];
		Matrix.setIdentityM(rot, 0);

		switch(action)
		{
			case MotionEvent.ACTION_MOVE:
				float dx = eventX - initX;
				tmpWorldRotateAngle = worldRotateAngle + dx * dxForOneTurn / 360.0f;

                game.getWorld().getCamera().rotateCameraY(tmpWorldRotateAngle);
		}

		return true;
	}

    public int getEffect()
    {
        return effect;
    }
}
