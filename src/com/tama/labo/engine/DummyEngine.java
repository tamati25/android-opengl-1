package com.tama.labo.engine;

import com.tama.labo.attributes.Position;
import com.tama.labo.game.Game;
import com.tama.labo.manager.Managers;
import com.tama.labo.shape.BasicParticle;
import com.tama.labo.shape.Particle;

public class DummyEngine extends Engine
{
	/*
	 * This engine does absolutely nothing. It is used when the debugging
	 * need to have a still scene.
	 */
	public DummyEngine(Managers managers)
	{
		super(managers);
	}

    public void init(Game game) {
        Particle p = new BasicParticle();
        p.setPos(new Position(0, 0, 0));
        managers.getShapeManager().create(p);
    }

	public void run()
	{
		while (isRunning != null && isRunning) {}
	}
}
