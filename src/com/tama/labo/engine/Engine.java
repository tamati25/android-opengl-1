package com.tama.labo.engine;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.view.MotionEvent;

import com.tama.labo.game.Game;
import com.tama.labo.interfaces.IEngine;
import com.tama.labo.manager.Managers;

public abstract class Engine implements IEngine
{
	protected Boolean isRunning;
    protected boolean useCustomLayout = false; //use default layout if not told otherwise
    protected int mCustomLayout;
    protected int orientation = ScreenOrientation.SCREEN_ORIENTATION_PORTRAIT;

    protected Managers managers;
    protected boolean alternateDrawing;

	public Engine(Managers managers)
	{
		isRunning = true;
        alternateDrawing = false;

        this.managers = managers;
	}

	public void stop()
	{
		finish();
		isRunning = false;
	}

	//Override this method to create a special scene or to initialize engine
	public abstract void init(Game game);

	//Override this method to set special parameters (see IEngine)
	public void setGLParameters(GL10 p1, EGLConfig p2) {}

	//Override this method to do things when the engine is stopped
	public void finish() {}

	//Override this method to handle touch events before the surface view
	public boolean onTouch(Game game, MotionEvent e) { return false; }

	//Override this method to handle touch events after the surface view
	public boolean onTouchAfter(Game game, MotionEvent e) { return false; }

    //Use a custom layout
    //The layout _must_ have a SurfaceView (extending GLSurfaceView) with id "view" or it will refuse to run.
    //It is also advised to include a TextView with id "text" if you plan to add debug information
    public boolean isUseCustomLayout() { return useCustomLayout; }
    public int getLayout() { return mCustomLayout; }

    //Engine can set their layout orientation, by default the engine will run locked in portrait mode
    //The engine has to define itself how it will behave when the device is in another orientation
    public int getOrientation() { return orientation; }

    public static final class ScreenOrientation
    {
        public static final int SCREEN_ORIENTATION_PORTRAIT = 0;
        public static final int SCREEN_ORIENTATION_LANDSCAPE = 1;
        public static final int SCREEN_ORIENTATION_BOTH = 2;
    }

    public Managers getManagers() { return managers; }

    public void setAlternateDrawing(boolean value) { this.alternateDrawing = value; }

    public boolean isAlternateDrawing() { return alternateDrawing; }
}
