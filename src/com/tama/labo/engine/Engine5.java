package com.tama.labo.engine;

import android.opengl.GLES20;

import com.tama.labo.attributes.Color;
import com.tama.labo.attributes.Position;
import com.tama.labo.game.Game;
import com.tama.labo.manager.Managers;
import com.tama.labo.shape.Rectangle;
import com.tama.labo.shape.TexturedParticle;
import com.tama.labo.util.Constants;
import com.tama.labo.world.Camera;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import static com.tama.labo.shape.Rectangle.*;

/**
 * Created by tama on 03/08/14.
 */
public class Engine5 extends Engine {

    public Engine5(Managers managers) {
        super(managers);
        alternateDrawing = true;
    }

    @Override
    public void setGLParameters(GL10 p1, EGLConfig p2)
    {
        GLES20.glClearColor(0.75f, 0.75f, 0.75f, 0.1f);
        GLES20.glEnable(GLES20.GL_DEPTH_TEST);
        GLES20.glEnable(GLES20.GL_BLEND);
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
        //GLES20.glBlendFunc(GLES20.GL_ZERO, GLES20.GL_SRC_COLOR);
    }

    @Override
    public void init(Game game) {

        //Set the world camera
        Camera camera = new Camera();
        camera.moveTo(Constants.RENDERER_INIT_EYE_X, Constants.RENDERER_INIT_EYE_Y, Constants.RENDERER_INIT_EYE_Z);
        camera.lookAt(0.0f, 0.0f, 0.0f);
        game.getWorld().setCamera(camera);

        Rectangle rectangle = new Rectangle();
        rectangle.setSize(2.0f);
        rectangle.setPos(new Position(-1, -1, -1));

        rectangle.setVertexColor(BOTTOM_LEFT_VERTEX, new Color(1.0f, 0.0f, 0.0f, 0.75f));
        rectangle.setVertexColor(BOTTOM_RIGHT_VERTEX, new Color(1.0f, 1.0f, 1.0f, 0.15f));
        rectangle.setVertexColor(TOP_LEFT_VERTEX, new Color(0.0f, 1.0f, 0.0f, 0.15f));
        rectangle.setVertexColor(TOP_RIGHT_VERTEX, new Color(0.0f, 0.0f, 1.0f, 0.15f));
        rectangle.setShaderToUse("BgShader");

        TexturedParticle tp = new TexturedParticle();
        tp.setUniformTexture(managers.getTextureManager().get("Chuuni"));
        tp.setSize(4.0f);
        tp.setPos(new Position(-0.01f, -0.01f, -10.0f));
        tp.setShaderToUse("TextureShader");

        managers.getShapeManager().create(rectangle);
        managers.getShapeManager().create(tp);
    }

    @Override
    public void run() {
        while (isRunning) { continue; };
    }
}
