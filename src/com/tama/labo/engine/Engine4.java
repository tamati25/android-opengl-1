package com.tama.labo.engine;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.opengl.GLES20;
import android.view.MotionEvent;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.tama.labo.attributes.Attribute;
import com.tama.labo.attributes.Color;
import com.tama.labo.game.Game;
import com.tama.labo.manager.Managers;
import com.tama.labo.opengl.MyRenderer;
import com.tama.labo.shape.TexturedField;
import com.tama.labo.util.Constants;
import com.tama.labo.util.DebugUtil;
import com.tama.labo.util.Util;

public class Engine4 extends Engine
{
    public Engine4(Managers managers)
    {
    	super(managers);

        //We want to use a custom layout !
        useCustomLayout = true;
        mCustomLayout = R.layout.engine4_layout;

        orientation = ScreenOrientation.SCREEN_ORIENTATION_LANDSCAPE;
    }

	@Override
	public void setGLParameters(GL10 p1, EGLConfig p2)
	{
		//Set background color : blue (3399FF) with alpha = 0.1
		Color bgColor = Color.getColor(0x33, 0x99, 0xFF, 0.1f);
		p1.glClearColor(bgColor.get(Attribute.RED), bgColor.get(Attribute.GREEN), bgColor.get(Attribute.BLUE), bgColor.get(Attribute.ALPHA));

		//Set the blending function, see http://i.stack.imgur.com/i2IAC.jpg for chart
		//(i.e http://gamedev.stackexchange.com/questions/29492/opengl-blending-help-needed)
		GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
	}

	@Override
	public void init(Game game)
	{
		//Set view
        game.getWorld().getCamera().moveTo(25.01f, 20f, 5.01f);
        game.getWorld().getCamera().lookAt(25f, 0f, -25f);

        managers.runOnUIThread(
                new Runnable() {
                    public void run() {
                        //Listener for checkbox
                        CheckBox restricted = (CheckBox) managers.getMainActivity().findViewById(R.id.restrictFps);

                        if (restricted != null) {
                            restricted.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                    if (isChecked)
                                        MyRenderer.setWantedFPS(2.0f);
                                    else
                                        MyRenderer.setWantedFPS(-1.0f);
                                }
                            });
                        } else {
                            DebugUtil.logD("Engine4", "Checkbox not found", Constants.DEBUG_LEVEL_STANDARD);
                        }
                    }
                });

		TexturedField tf1 = new TexturedField(managers.getTextureManager().get("Chuuni"));
		tf1.setShaderToUse("TextureShader");

		float[] heightMap = Util.loadHeightMap(managers.getMainActivity(), "heightMaps/hm2.png", 50, 50, -5, 10);
		tf1.setHeightMap(heightMap);
        managers.getShapeManager().create(tf1);
	}

	public void run()
	{
		while (isRunning != null && isRunning) {
        }
	}

	@Override
	public boolean onTouch(Game game, MotionEvent e)
	{
//		TODO : check merge...
//		float eventX = e.getX();
//		int action = e.getActionMasked();
//		boolean out = false; //don't consume event
//		switch(action)
//		{
//			case MotionEvent.ACTION_DOWN:
//			  initX = eventX;
//			  break;
//
//			case MotionEvent.ACTION_UP:
//			  worldRotateAngle = tmpWorldRotateAngle;
//			  break;
//		}
//		return out;
		return false;
	}

	@Override
	public boolean onTouchAfter(Game game, MotionEvent e)
	{
		//We test "move" event after the zoom event, to avoid zooming and moving at the
		//same time (the zoom event will be consumed in the surface view)
		int action = e.getActionMasked();
		//float eventX = e.getX();
		
		switch(action)
		{
			//TODO : check merge
		}

		return true;
	}

}
