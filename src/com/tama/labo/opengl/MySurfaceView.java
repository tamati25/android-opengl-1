package com.tama.labo.opengl;
import android.content.Context;
import android.graphics.PixelFormat;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

import com.tama.labo.game.Game;
import com.tama.labo.game.MainActivity;
import com.tama.labo.util.Constants;
import com.tama.labo.util.DebugUtil;
import com.tama.labo.util.MathUtil;

public class MySurfaceView extends GLSurfaceView
{
	private MyRenderer mRenderer;
    private Game game;

//  private float refX;

	private float baseSpacing = 0;
	private boolean isZooming = false;

	public MySurfaceView(Context context, AttributeSet attrs)
	{
		super(context, attrs);

		setEGLContextClientVersion(2);
		setEGLConfigChooser(8 , 8, 8, 8, 16, 0);
        getHolder().setFormat(PixelFormat.RGB_565);
	}

	public void refresh(Game game)
	{
		Log.d("MySurfaceView", "refresh");

        this.game = game;
		mRenderer = new MyRenderer(game);
		setRenderer(mRenderer);
		setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
	}

	public MyRenderer getRenderer()
	{
		return mRenderer;
	}

	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
		// Priority is given to the engine handling of touch events
		if (game.getEngine().onTouch(game, e)) return false;

		float sqDist;
		//float x = e.getX();

		int action = e.getAction();

		switch (action & MotionEvent.ACTION_MASK)
		{
			case MotionEvent.ACTION_POINTER_DOWN:
				Log.d("MySurfaceView", "Action pointer down.");
				if (e.getPointerCount() > 1)
				{
					sqDist = MathUtil.getSquareDistanceBetween(e.getX(0), e.getY(0), e.getX(1), e.getY(1));
					if (sqDist > 100) //10 * 10, duh.
					{
						isZooming = true;
						baseSpacing = sqDist;
						Log.d("MySurfaceView", "Set base spacing to " + baseSpacing);
					}
				}
				break;

			case MotionEvent.ACTION_POINTER_UP:
				isZooming = false;
				break;

			default: break;
		}

		if (action == MotionEvent.ACTION_MOVE)
		{
			Log.d("MySurfaceView", "MotionEvent.ACTION_MOVE detected");

            if (isZooming)
			{
				//Handle zoom / de-zoom here
				sqDist = MathUtil.getSquareDistanceBetween(e.getX(0), e.getY(0), e.getX(1), e.getY(1));

				float zoom = MathUtil.keepBetween(Constants.ZOOM_MIN, sqDist /baseSpacing, Constants.ZOOM_MAX);

				if (DebugUtil.debugPrint(Constants.DEBUG_LEVEL_MORE_VERBOSE))
					Log.d("MySurfaceView", "Zoom x " + zoom);

                game.getWorld().getCamera().zoom(zoom);
				return true;
			}
		}
		else if (action == MotionEvent.ACTION_DOWN)
		{
			//TODO : merge fail ? (probably to rotate world.)
			//refX = x;
			return true;
		}

		//If we get here, it means that the event was still not consumed
		//so we can call the Engine.onTouchAfter event to see if it can
		//still trigger something
		return game.getEngine().onTouchAfter(game, e);
	}
	
}