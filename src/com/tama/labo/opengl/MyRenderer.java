package com.tama.labo.opengl;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.app.Activity;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.util.Log;

import com.tama.labo.game.Game;
import com.tama.labo.game.MainActivity;
import com.tama.labo.interfaces.IPredicate;
import com.tama.labo.shape.Shape;
import com.tama.labo.util.Constants;
import com.tama.labo.util.DebugUtil;

import java.util.List;

public class MyRenderer implements GLSurfaceView.Renderer
{
    private static float wantedFPS = 60; //default fps = 60fps
    private static float time_per_frame = 1000000000.0f/wantedFPS;

	/* fps count variables (for debug mode only) */
    private long lastFrameTimeStamp; //when did the last frame finished drawing ?
    private long elapsedSinceLastFrame; //time elapsed since the last frame finished drawing
	private final int sampleFrameCount = 5; //How many frames do we need ?
	private float lastFpsCount = 0;
	private final long[] frames = new long[sampleFrameCount];
    private long cumul = 0;
    private float fps;
	private float cumulFps = 0;
	private float meanFps;
    private int frameCount = 0;

    private Game game;
    
    public MyRenderer(Game game)
    {
        this.game = game;
    }

	public void onSurfaceCreated(GL10 p1, EGLConfig p2)
	{
		Log.d("MyRenderer", "onSurfaceCreated");

		// No culling of back faces
		GLES20.glDisable(GLES20.GL_CULL_FACE);

		// No depth testing
		GLES20.glDisable(GLES20.GL_DEPTH_TEST);

		// Enable blending
		GLES20.glEnable(GLES20.GL_BLEND);

		//Set the blending function, see http://i.stack.imgur.com/i2IAC.jpg for chart
		//(i.e http://gamedev.stackexchange.com/questions/29492/opengl-blending-help-needed)
		GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE);

		p1.glClearColor(0.1f, 0.1f, 0.1f, 0.1f);

        game.initTextures();

		// Ask the engine if it needs to override default GL parameters set above
		game.getEngine().setGLParameters(p1, p2);

        // Initialize engine (set scene...)
		game.getEngine().init(game);

        lastFrameTimeStamp = System.nanoTime();

        //Aschente :D
        game.start();
    }

	  public void onDrawFrame(GL10 gl)
	  {
          if (wantedFPS > 0)
          {
              elapsedSinceLastFrame = System.nanoTime() - lastFrameTimeStamp;
              if (elapsedSinceLastFrame < time_per_frame)   return; //don't draw yet.
          }

          //http://stackoverflow.com/questions/10042412/glenablegl-depth-test-nothing-rendered
		  GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

          // -- Measure FPS method :
          //we want to measure the time elapsed since the last frame began drawing i.e
          //
          // ______________________________________________
          // |Drawing |      Waiting next frame   | Drawing|
          // |________|___________________________|________|
          // |________|___________________________|________|
          // <------------------------------------>
          //    Measure this time to determine fps
          //
          // See comment below for the method to compute fps by sampling N frames
          //
          long thisFrameDrawTime = (long) ((System.nanoTime() - lastFrameTimeStamp) * 0.000001f);
          lastFrameTimeStamp = System.nanoTime();

		  synchronized(game.getEngine().getManagers().getShapeManager().getAll())
		  {
			  if (DebugUtil.debugPrint(Constants.DEBUG_LEVEL_VERBOSE))
				  Log.d("MyRenderer", "Drawing shapes");

              if (!game.getEngine().isAlternateDrawing()) {
                  game.getWorld().render(null);
              }
              else
              {
                  //Alternate drawing method : http://stackoverflow.com/a/3390094
                  List<Shape> opaqueShapes = game.getEngine().getManagers().getShapeManager().where(new IPredicate<Shape>() {
                      @Override
                      public boolean check(Shape object) {
                          return (object.getAlpha() == 1);
                      }
                  });
                  game.getWorld().render(opaqueShapes);

                  List<Shape> transparentShapes = game.getEngine().getManagers().getShapeManager().where(new IPredicate<Shape>() {
                      @Override
                      public boolean check(Shape object) {
                          return (object.getAlpha() != 1);
                      }
                  });
                  game.getWorld().render(transparentShapes);
              }

			  if (DebugUtil.debugPrint(Constants.DEBUG_LEVEL_VERBOSE))
				  Log.d("MyRenderer", "Drawing shapes end");
		  }

          if (DebugUtil.isDebugMode)
		  {
              if (frameCount < sampleFrameCount)
			  {
				frames[frameCount] = thisFrameDrawTime;
			  }
			  else
			  {
				  cumul -= frames[0]; //substract the oldest frame saved
                  System.arraycopy(frames, 1, frames, 0, sampleFrameCount - 1);
				  frames[sampleFrameCount - 1] = thisFrameDrawTime;
			  }
			  cumul += thisFrameDrawTime;

			  frameCount++;

			  //sample frames took X ms to draw, so we can draw (sample * 1000)/X per second
			  //with the last fps count taken into account (10% - make fps more stable),
			  //that becomes 0.9 * sample * 1000 = 900 * sample (i.e fps = 90% current fps + 10% last fps measured)
			  float total = Math.min(frameCount, sampleFrameCount) * 900.0f; //When drawing the first frames, adjust the formula...
			  float trueFps = Math.min(total/cumul, 90); //fps is capped at 90 max (should be enough)
			  fps = trueFps + 0.1f * lastFpsCount;

			  cumulFps += trueFps * 1.11; // 10/9 = 1.111... we can afford losing some precision
			  meanFps = cumulFps / frameCount;

			  if (DebugUtil.debugPrint(Constants.DEBUG_LEVEL_MORE_VERBOSE))
			  {
				  Log.d("MyRenderer", "elapsed : " + thisFrameDrawTime);
				  Log.d("MyRenderer", "cumul : " + cumul);
				  Log.d("MyRenderer", "trueFps : " + trueFps);
				  Log.d("MyRenderer", "cumulFps : " + cumulFps);
				  Log.d("MyRenderer", "lastFps : " + lastFpsCount);
				  Log.d("MyRenderer", "result => " + fps);
			  }

              final long meanTimePerFrame = cumul/sampleFrameCount;

			  game.getEngine().getManagers().runOnUIThread(
					  new Runnable() {
						  public void run()
						  {
							  //round the number to 3 decimal places
							  //from http://stackoverflow.com/questions/153724/how-to-round-a-number-to-n-decimal-places-in-java
							  game.getEngine().getManagers().getMainActivity().setText(String.format("%.3g [ %d ] [ %.4g ] [%d ms]", fps, frameCount, meanFps, meanTimePerFrame));
						  }
					  });

			  lastFpsCount = (float)Math.max(fps, 0.01);
		  }
	  }

	public void onSurfaceChanged(GL10 p1, int width, int height)
	{
        float[] mProjMatrix = new float[16];
        Matrix.setIdentityM(mProjMatrix, 0);

		//set camera & view here
		GLES20.glViewport(0, 0, width,height);
		float ratio = (float)width/height;
		Matrix.frustumM(mProjMatrix, 0, -ratio, ratio, -1f, 1f, 1f, 100.0f);
        game.getWorld().setProjMatrix(mProjMatrix);

    }

	/*
	 * Getters
	 */

    public static void setWantedFPS(float fps)
    {
        //set to a negative number (<= 0) if you don't want to restrict fps
        wantedFPS = fps;

        //refresh the time needed between frames
        time_per_frame = 1000000000.0f/wantedFPS;
    }
}
