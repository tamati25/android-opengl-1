package com.tama.labo.game;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

import android.util.Log;

import com.tama.labo.engine.Engine;
import com.tama.labo.manager.Managers;
import com.tama.labo.manager.TextureManager;
import com.tama.labo.shader.Shader;
import com.tama.labo.shape.Shape;
import com.tama.labo.world.World;

public class Game
{
	private Thread engineThread = null;

    private World world;
    private Engine engine;

    public Game(Engine engine, World world)
    {
        this.engine = engine;
    	this.world = world;
    }

    public ConcurrentHashMap<Shader, ArrayList<Shape>> getShapes()
    {
        ConcurrentHashMap<Shader, ArrayList<Shape>> shapes = null;

        if (world != null)
            shapes = engine.getManagers().getShapeManager().getAll();

        return shapes;
    }

    public void start()
    {
        engineThread = new Thread(engine);
        engineThread.start();
    }

	public void stop()
	{
        if (world != null)
            world.clear();

		if (engine != null)
		{
			engine.stop();

			try {
				Log.d("OpenGL", "Waiting for engine thread to stop...");

				if (engineThread != null)
					engineThread.join();

				Log.d("OpenGL", "Thread stopped :)");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

    /* -- Getters -- */
    public World getWorld() { return world; }
    public Engine getEngine() { return engine; }
    public Game setEngine(Engine engine) { this.engine = engine; return this; }

    public void initTextures()
    {
        //Register textures
        TextureManager tm = engine.getManagers().getTextureManager();
        tm.register("Glass", tm.create(engine.getManagers().getMainActivity(), "glass"));
        tm.register("Android", tm.create(engine.getManagers().getMainActivity(), "android"));
        tm.register("Chuuni", tm.create(engine.getManagers().getMainActivity(), "chuuni"));
    }
}
