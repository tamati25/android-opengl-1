package com.tama.labo.game;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.tama.labo.engine.*;
import com.tama.labo.manager.Managers;
import com.tama.labo.manager.ShapeManager;
import com.tama.labo.net.FileDownloader;
import com.tama.labo.opengl.MySurfaceView;
import com.tama.labo.opengl.R;
import com.tama.labo.util.DebugUtil;
import com.tama.labo.world.Camera;
import com.tama.labo.world.World;

public class MainActivity extends Activity {
	private ProgressDialog mDialog;

	private MySurfaceView mySurfaceView = null;

	private TextView text;

	private Game game;

    private Managers managers;

    private Engine engine;

    private World world;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (!check_files()) {
			mDialog = new ProgressDialog(MainActivity.this);
			mDialog.setMessage("Downloading missing files...");
			mDialog.setIndeterminate(false);
			mDialog.setMax(100);
			mDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

			FileDownloader downloader = new FileDownloader(this);

			// sample obj file
			downloader
					.execute("https://s3-ap-northeast-1.amazonaws.com/tamatestbucket/labo/test.obj");
		}
	}

	@Override
	public void onStart() {
		super.onStart();

		Log.d("MainActivity", "onStart");
		startGame();
	}

	@Override
	public void onRestart() {
		super.onRestart();

		Log.d("MainActivity", "onRestart");
		startGame();
	}

	@Override
	public void onStop() {
		super.onStop();

		Log.d("MainActivity", "onStop");
		endGame();
	}

	@Override
	public void onPause() {
		super.onPause();

		Log.d("MainActivity", "onPause");
		endGame();
	}

	public void startGame()
    {
        Log.d("MainActivity", "Starting game");

        managers = new Managers(this);

        engine = new Engine5(managers);
    	world = new World(managers);
        game = new Game(engine, world);

        switch (engine.getOrientation())
        {
            case Engine.ScreenOrientation.SCREEN_ORIENTATION_PORTRAIT:
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                break;
            case Engine.ScreenOrientation.SCREEN_ORIENTATION_LANDSCAPE:
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                break;
            default:
                break;
        }

        if (engine.isUseCustomLayout())
        {
            setContentView(engine.getLayout());
        }
        else
        {
    	    if (DebugUtil.isDebugMode)
    		    setContentView(R.layout.debug_main);
    	    else
    		    setContentView(R.layout.main);
        }

		text = (TextView)findViewById(R.id.text);

        if (text == null) //the text view found for debug was not found, disable debug mode
            DebugUtil.isDebugMode = false;

		mySurfaceView = (MySurfaceView)findViewById(R.id.view);

        if (mySurfaceView == null)
            throw new RuntimeException("SurfaceView not found in layout, can't continue !");

        mySurfaceView.refresh(game);
    }

	private void endGame() {
		// end engine and invalidate view
		if (mySurfaceView != null)
			mySurfaceView.onPause();

		game.stop();
	}
	
	@Override
	public void onConfigurationChanged(Configuration configuration) {
		endGame();
		startGame();
	}

	/**
	 * Check if the files are present, if not return false
	 */
	public boolean check_files() {
		return true;
	}

	/**
	 * Getters / setters
	 */
	public ProgressDialog getProgressDialog() {
		return mDialog;
	}

	public MySurfaceView getView() {
		return mySurfaceView;
	}

	public TextView getText() {
		return text;
	}

	public void setText(String txt) {
		if (text != null) // the text view is only available in debug mode
			text.setText(txt);
	}
	
	public Game getGame() {
		return game;
	}

    public Managers getManagers() { return managers; }

    public Engine getEngine() { return engine; }

    public World getWorld() { return world; }
}
