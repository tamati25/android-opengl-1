package com.tama.labo.net;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.tama.labo.game.MainActivity;
import com.tama.labo.util.Constants;
import com.tama.labo.util.DebugUtil;

public class FileDownloader extends AsyncTask<String, Integer, Void>
{
	/* Download a file from the URL given */

	private MainActivity mainActivity;

	public FileDownloader(MainActivity mainActivity)
	{
		this.mainActivity = mainActivity;
	}

	@Override
	protected Void doInBackground(String... urls) {
		//extract the last part of the URL (after the last '/') to get the file name
		//if not present, take the complete string (but that shouldn't happen...)
		String[] splitUrl = urls[0].split("/");
		String downloadedFileName = splitUrl[splitUrl.length - 1];

		try {
			URL fileUrl = new URL(urls[0]);
			URLConnection connection = fileUrl.openConnection();
			connection.connect();

			int fileLength = connection.getContentLength();

			if (DebugUtil.debugPrint(Constants.DEBUG_LEVEL_MORE_VERBOSE))
				Log.d("FileDownloader", "File length : " + fileLength);

			//Download me ! (see http://stackoverflow.com/questions/3028306)
			InputStream input = new BufferedInputStream(fileUrl.openStream());

			FileOutputStream fos = mainActivity.openFileOutput(downloadedFileName, Context.MODE_PRIVATE);

			byte data[] = new byte[1024];
			long downloaded = 0;
			int count;

			while ((count = input.read(data)) > 0)
			{
				downloaded += count;

				if (DebugUtil.debugPrint(Constants.DEBUG_LEVEL_MOST_VERBOSE))
					Log.d("FileDownloader", new String(data));

				publishProgress((int) (downloaded * 100 / fileLength));
				fos.write(data, 0, count);
			}

			//Data is downloaded, clean resources
			fos.close();
			input.close();

		} catch (MalformedURLException e) {
			Log.w("FileDownloader", "MalformedURLException catched !");
		} catch (IOException e) {
			Log.w("FileDownloader", "IOException catched !");
		}

		return null;
	}

	@Override
	protected void onPreExecute()
	{
		super.onPreExecute();

		mainActivity.runOnUiThread(new Runnable() {
			public void run()
			{
				mainActivity.getProgressDialog().show();
			}
		});
	}

	@Override
	protected void onProgressUpdate(final Integer... progress)
	{
		super.onProgressUpdate(progress);

		mainActivity.runOnUiThread(new Runnable() {
			public void run()
			{
				mainActivity.getProgressDialog().setProgress(progress[0]);
			}
		});
	}

	@Override
	protected void onPostExecute(Void result)
	{
		super.onPostExecute(result);

		mainActivity.runOnUiThread(new Runnable() {
			public void run()
			{
				mainActivity.getProgressDialog().dismiss();
			}
		});

		//start game !
		mainActivity.startGame();
	}
}
