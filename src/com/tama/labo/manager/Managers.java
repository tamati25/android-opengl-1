package com.tama.labo.manager;


import com.tama.labo.game.MainActivity;

/**
 * Created by tama on 24/07/14.
 *
 * Managers class containing all the managers needed
 */
public class Managers
{
    /**
     * Main activity reference
     */
    private MainActivity mainActivity;

    /**
     * Shader manager
     */
    private ShaderManager shaderManager;

    /**
     * Texture manager
     */
    private TextureManager textureManager;

    /**
     * Shape manager
     */
    private ShapeManager shapeManager;

    /**
     * Constructor
     * @param mainActivity a reference to the main activity
     */
	public Managers(MainActivity mainActivity)
	{
        this.mainActivity = mainActivity;
		this.shaderManager = new ShaderManager();
		this.textureManager = new TextureManager();
        this.shapeManager = new ShapeManager(mainActivity);
	}

    /**
     * Run code on the UI thread
     * @param r the code to run
     */
    public void runOnUIThread(Runnable r)
    {
        mainActivity.runOnUiThread(r);
    }

    /**
     * Get shader manager
     * @return the shader manager
     */
    public ShaderManager getShaderManager()
    {
        return shaderManager;
    }

    /**
     * Get texture manager
     * @return the texture manager
     */
    public TextureManager getTextureManager()
    {
        return textureManager;
    }

    /**
     * Get shape manager
     * @return the shape manager
     */
    public ShapeManager getShapeManager() { return shapeManager; }

    /**
     * Get main activity
     * @return the main activity
     */
    public MainActivity getMainActivity() { return mainActivity; }
}
