package com.tama.labo.manager;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.util.Log;

public class TextureManager extends AbstractManager<String, Integer>
{
    private static int index = 0;

    /**
     * Create a texture
     * @param context a reference to the context (MainActivity)
     * @param fileName the file name containing the texture to be used
     * @return an integer referencing the texture used, or null if there was an error
     */
    @Override
	public Integer create(Object...params)
	{
        Context context = null;
        String fileName = null;

        if (params != null)
        {
            context = (Context) params[0];
            fileName = (String) params[1];
        }

        if (index >= 32)
            return null;

		//Create the texture from the file name given
		//from http://www.learnopengles.com/android-lesson-four-introducing-basic-texturing

        GLES20.glActiveTexture(GLES20.GL_TEXTURE0 + index);
		final int[] textureHandle = new int[1];
		GLES20.glGenTextures(1, textureHandle, 0);

		if (textureHandle[0] != 0)
		{
			final BitmapFactory.Options options = new BitmapFactory.Options();
			options.inScaled = false;

			// Read resource
		    Bitmap bitmap = null;

            try {
                int resourceId = context.getResources().getIdentifier(fileName, "drawable", context.getPackageName());
                bitmap = BitmapFactory.decodeResource(context.getResources(), resourceId, options);

                Log.d("OpenGL1", "-----------------------------------------------------------");
                Log.d("OpenGL1", String.valueOf(bitmap.getHeight()));
                Log.d("OpenGL1", String.valueOf(bitmap.getWidth()));
                Log.d("OpenGL1", "-----------------------------------------------------------");

            } catch (Resources.NotFoundException e) {
                Log.w("TextureManager", "Can't open resource : not found.");
            }

            if (bitmap != null) {
                // Bind texture
                GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureHandle[0]);

                // Set filtering
                GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
                GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);
                GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
                GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);

                // Load the bitmap into the bound texture
                GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);

                // Recycle bitmap
                bitmap.recycle();
            }
        }
		else
			throw new RuntimeException("Error creating texture");

		return textureHandle[0];
	}

    /**
     * Register a texture
     * @param name the texture name
     * @param texId the integer returned by the create method
     */
    @Override
	public void register(String name, Integer texId)
    {
        super.register(name, index);
        index++;

        Log.d("TextureManager", "Registered : (" + name + ", " + texId + ")");
    }
}
