package com.tama.labo.manager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import android.opengl.GLSurfaceView;
import android.util.Log;

import com.tama.labo.game.Game;
import com.tama.labo.game.MainActivity;
import com.tama.labo.interfaces.IMap;
import com.tama.labo.interfaces.IMethod;
import com.tama.labo.interfaces.IPredicate;
import com.tama.labo.shader.Shader;
import com.tama.labo.shape.Shape;
import com.tama.labo.util.Constants;

public class ShapeManager extends AbstractManager<Shader, ArrayList<Shape>>
{
    /**
     * A reference to the main activity
     */
    private MainActivity mainActivity = null;

    /**
     * Map counting the number of shapes by their type (their class)
     */
    private HashMap<Class<?>, ArrayList<Shape>> shapesByType;

    /**
     * Constructor
     * @param mainActivity a reference to the main activity
     */
    public ShapeManager(MainActivity mainActivity)
    {
        this.mainActivity = mainActivity;
        shapesByType = new HashMap<Class<?>, ArrayList<Shape>>();
    }

    /**
     * Initialize a shape with the shader to use
     * @param shape the uninitialized shape to initialize
     * @param shaderName the shader name to use (will be initialized if necessary)
     * @return the initialized shape if successful, else null
     */
	public ArrayList<Shape> create(Object... objects)
	{
        final Shape shape;
        final String shaderName;

        if (objects == null)
            return null;

        shape = (Shape) objects[0];

        if (shape.getShaderToUse() == null)
            shaderName = Constants.DEFAULT_SHADER_NAME;
        else
            shaderName = shape.getShaderToUse();

		Log.d("ShapeManager#createShape", "Creating " + shape.getClass().getCanonicalName() + " with shader " + shaderName);

		synchronized (registered)
		{
            GLSurfaceView v = mainActivity.getView();

            v.queueEvent(new Runnable() {
                public void run() {
                    if (shape.initialize(mainActivity, shaderName))
                        onCreateNewShape(shape);
                }
            });
        }
        return null;
	}

    /**
     * Destroy a shape previously registered (un-register it)
     * @param shape the shape to destroy
     */
	public void destroyShape(final Shape shape)
	{
		if (shape == null)  return;

		Shader shader = shape.getShader();

		if (shader == null)
		{
			Log.w("destroyShape", "No shader set, can't destroy shape");
			return;
		}

		synchronized (registered)
		{
			//Update the list
			ArrayList<Shape> shapesWithShader = registered.get(shader);

			if (shapesWithShader == null)
			{
				Log.w("destroyShape", "Shader not found, aborting.");
				return;
			}

			shapesWithShader.remove(shape);
			onDestroyShape(shape);
		}
	}

    /**
     * Method called when a new shape is created
     * @param s the shape created
     */
	private void onCreateNewShape(Shape s)
	{
		if (s == null)  return;

		Class<?> c = s.getClass();

		if (!shapesByType.containsKey(c))
			shapesByType.put(c, new ArrayList<Shape>());

		shapesByType.get(c).add(s);
	}

    /**
     * Method called when a shape is destroyed
     * @param s the shape destroyed
     */
	private void onDestroyShape(Shape s)
	{
		Class<?> c = s.getClass();

		ArrayList<Shape> registered = shapesByType.get(c);

		if (registered == null)
		{
			Log.w("ShapeManager", "onDestroyShape : No shape of this type was registered before");
			return;
		}

		//Update the list
		registered.remove(s);
		shapesByType.put(c, registered);
	}

    /**
     * Get the number of shapes with the type given
     * @param c the type of the shape
     * @return the number of shapes of this type
     */
    public int getShapeCount(Class<?> c)
    {
        int result = 0;

        if (shapesByType.get(c) != null)
            result = shapesByType.get(c).size();

        return result;
    }

    /**
     * Register a shape
     * @param s the shape (initialized)
     * @param shader the shader used to initialize the shape
     */
    public void registerShapeWithShader(Shape s, Shader shader)
    {
        synchronized(registered)
        {
            ArrayList<Shape> shapeList;

            if (!registered.containsKey(shader))  //You must be new.
                shapeList = new ArrayList<Shape>();
            else
                shapeList = registered.get(shader);

            shapeList.add(s);
            registered.put(shader, shapeList);
        }
    }

    /**
     * Run some code on all the shapes registered
     * @param method the method to run
     */
    public void applyAll(IMap<Shape> method)
    {
        applyAll(method, null, null);
    }

    /**
     * Run some code on all the shapes registered
     * @param method the method to run
     * @param applyBefore code that will run before each shader switch
     */
    public void applyAll(IMap<Shape> method, IMethod applyBefore)
    {
        applyAll(method, applyBefore, null);
    }

    /**
     * Run some code on all the shapes registered
     * @param method the method to run
     * @param applyBefore code that will run before each shader switch
     * @param applyAfter code that will run at the end of each shader
     */
    public void applyAll(IMap<Shape> method, IMethod applyBefore, IMethod applyAfter)
    {
        for (Shader shader : registered.keySet()) {

            if (applyBefore != null)
                applyBefore.run(shader);

            //Time to draw, get the shape list and go !
            ArrayList<Shape> shapeList = registered.get(shader);

            for (Shape s : shapeList) {
                method.apply(s);
            }

            if (applyAfter != null)
                applyAfter.run(shader);
        }
    }

    public List<Shape> where(IPredicate<Shape> predicate)
    {
        List<Shape> out = new ArrayList<Shape>();

        for (Shader shader : registered.keySet()) {

            ArrayList<Shape> shapeList = registered.get(shader);

            for (Shape s : shapeList) {
                if (predicate.check(s))
                    out.add(s);
            }
        }

        return out;
    }
}
