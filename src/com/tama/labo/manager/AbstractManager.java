package com.tama.labo.manager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import android.util.Log;

import com.tama.labo.interfaces.IPredicate;

/**
 * Created by tama on 24/07/14.
 *
 * Representing a manager with methods to create an object, register it, get it...
 */
public abstract class AbstractManager<KeyType, Type>
{
    /**
     * Internal object representing the registered objects
     */
    protected ConcurrentHashMap<KeyType, Type> registered = new ConcurrentHashMap<KeyType, Type>();

    /**
     * Create an object (subclasses need to override this method)
     * @param params parameters needed to create the object
     * @return the object created
     */
    public abstract Type create(Object... params);

    /**
     * Add an object to the list of registered objects
     * @param key the key that will be used to retrieve the object registered
     * @param value the object to register
     */
    public void register(KeyType key, Type value)
    {
        if (key == null || value == null)
            Log.w("Manager", "Can't register null values");

        if (isRegistered(key))
            Log.w("Manager", "Overwriting existing value");

        registered.put(key, value);
    }

    /**
     * Get an objet previously registered by its key
     * @param key the key that was used when the object was registered
     * @return the object corresponding to the key given, or null if not found
     */
    public Type get(KeyType key)
    {
        return registered.get(key);
    }

    /**
     * Get all the objects registered
     * @return all the objects registered
     */
    public ConcurrentHashMap<KeyType, Type> getAll() { return registered; }

    /**
     * Check if an object with the key given was registered
     * @param key the key
     * @return true if an object was registered with this key before
     */
    public boolean isRegistered(KeyType key)
    {
        return registered.containsKey(key);
    }

    /**
     * Delete all the objects registered
     */
    public void clear() { registered.clear(); }

    /**
     * Get the default object (usually the first object registered)
     * @return the first object found or null if no object was registered before
     */
    public Type getDefault()
    {
        Type result = null;

        Set<KeyType> keys = registered.keySet();
        if (keys != null && keys.size() > 0)
        {
            KeyType firstKey = keys.iterator().next();
            result = registered.get(firstKey);
        }

        return result;
    }
}
