package com.tama.labo.manager;

import android.util.Log;

import com.tama.labo.game.MainActivity;
import com.tama.labo.shader.Shader;

public class ShaderManager extends AbstractManager<String, Shader>
{
    //Where are the shaders ? (default com.tama.labo.shader)
    private final static String shadersPackage = "com.tama.labo.shader";

    /**
     * Create a shader by instantiating the class with the name given, and initialize it if possible
     * @param mainActivity : a reference to the main activity (used to initialize shader)
     * @param name : shader name to create
     * @return the created shader if created successfully, else null.
     */
    @Override
	public Shader create(Object... params)
	{
        if (params == null)
            return null;

        final MainActivity mainActivity = (MainActivity) params[0];
        final String name = (String) params[1];

		String fullName = shadersPackage + "." + name;
		Shader out = null;

		try {
			Class<?> c = Class.forName(fullName);
			out = (Shader) c.newInstance(); //The shader *must* define a default constructor
											//with no parameters

			if (out != null) //we got our shader, now initialize it !
				out.initShader(mainActivity);
			else
				Log.w("ShaderManager", "Instance is null ?");

		} catch (ClassNotFoundException e) {
			Log.w("ShaderManager", "Class " + fullName + " not found.");
		} catch (InstantiationException e) {
			Log.w("ShaderManager", "InstantiationException : " + e.getMessage());
		} catch (IllegalAccessException e) {
			Log.w("ShaderManager", "IllegalAccessException : " + e.getMessage());
		}

		return out;
	}
}
