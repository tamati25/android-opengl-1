package com.tama.labo.shader;


import com.tama.labo.game.MainActivity;

public abstract class Shader
{
	protected int mProgram;

	protected int vertexShader;
	protected int fragmentShader;

	public Shader() {}

	//The subclasses have to initialize the shader themselves
	public abstract void initShader(MainActivity mainActivity);


	public int getmProgram()
	{
		return mProgram;
	}

	//This method needs to be implemented in the sub classes because the
	//shader manager needs the name to be able to tell if the shader was
	//already registered
	public abstract String getName();
}
