package com.tama.labo.shader;

import android.opengl.GLES20;
import android.util.Log;

import com.tama.labo.game.MainActivity;
import com.tama.labo.util.ShaderUtil;

public class BasicShader extends Shader
{

    public BasicShader()
	{
		super();
	}

	@Override
	public String getName() {
		return "BasicShader";
	}

	@Override
	public void initShader(MainActivity mainActivity)
	{
		Log.d("BasicShader", "initShader");

		mProgram = GLES20.glCreateProgram();
        String vertexShaderFileName = "shaders/BasicVertexShader.fx";
        vertexShader = ShaderUtil.loadShader(this, GLES20.GL_VERTEX_SHADER, vertexShaderFileName, mainActivity);
        String fragmentShaderFileName = "shaders/BasicFragmentShader.fx";
        fragmentShader = ShaderUtil.loadShader(this, GLES20.GL_FRAGMENT_SHADER, fragmentShaderFileName, mainActivity);

		GLES20.glAttachShader(mProgram, vertexShader);
		GLES20.glAttachShader(mProgram, fragmentShader);
		GLES20.glBindAttribLocation(mProgram, 0, "a_Position");
		GLES20.glBindAttribLocation(mProgram, 1, "a_Color");
		GLES20.glLinkProgram(mProgram);
	}
}
