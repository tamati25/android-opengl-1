package com.tama.labo.shader;

import android.opengl.GLES20;
import android.util.Log;

import com.tama.labo.game.MainActivity;
import com.tama.labo.util.ShaderUtil;

public class TextureShader extends Shader
{
    public TextureShader()
	{
		super();
	}

	@Override
	public String getName() {
		return "TextureShader";
	}

	@Override
	public void initShader(MainActivity mainActivity) {
		Log.d("TextureShader", "initShader");

		mProgram = GLES20.glCreateProgram();
		Log.d("TextureShader", "Created program id :" + mProgram);
        String vertexShaderFileName = "shaders/TextureVertexShader.fx";
        vertexShader = ShaderUtil.loadShader(this, GLES20.GL_VERTEX_SHADER, vertexShaderFileName, mainActivity);
        String fragmentShaderFileName = "shaders/TextureFragmentShader.fx";
        fragmentShader = ShaderUtil.loadShader(this, GLES20.GL_FRAGMENT_SHADER, fragmentShaderFileName, mainActivity);

		GLES20.glAttachShader(mProgram, vertexShader);
		GLES20.glAttachShader(mProgram, fragmentShader);
		GLES20.glBindAttribLocation(mProgram, 0, "a_Position");
		GLES20.glBindAttribLocation(mProgram, 1, "a_Color");
		GLES20.glBindAttribLocation(mProgram, 2, "a_TexCoordinate");
		GLES20.glLinkProgram(mProgram);
	}
}
