package com.tama.labo.parser;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;

import android.content.Context;
import android.util.Log;

import com.tama.labo.util.Constants;
import com.tama.labo.util.DebugUtil;
import com.tama.labo.util.Util;

public class ObjParser
{
	/*
	 * This class parses an obj file exported from Blender and creates
	 * the buffers from the data inside it. Instantiate a GenericShape
	 * with the file name to use it
	 */

	// TODO create a Face (inner ?) class to store informations

	// Input stream for reading file
	private InputStream is = null;

	// Buffers (vertex buffer, colors buffer, index buffer)
	private final ArrayList<Float> vertexBuffer = new ArrayList<Float>();
	private final ArrayList<Float> colorsBuffer = new ArrayList<Float>();
//	private ArrayList<Short> indexBuffer = new ArrayList<Short>();

	private FloatBuffer mVertexBuffer;
	private FloatBuffer mColorsBuffer;
	private ShortBuffer mIndexBuffer;

	// Keep track of the length of each buffer, to be able to draw correctly
	private int numberOfVertices = -1;

	public ObjParser(String fileName, Context context, boolean isDownloadedFile)
	{
		try {
			if (isDownloadedFile)
				is = context.openFileInput(fileName); //open file from internal storage
			else
				is = context.getAssets().open(fileName); //open file from assets folder

		} catch (IOException e) {
			Log.w("ObjParser", "File not found, parse method will not work");
		}
	}

	public void parse()
	{
		if (is == null)
		{
			Log.w("ObjParser", "is = null, aborting.");
			return;
		}

		DebugUtil.logD("ObjParser", "Dumping data", Constants.DEBUG_LEVEL_STANDARD);

		String rawData = Util.getRawData(is);

		if (rawData != null)
		{
			String[] lines = rawData.split("\n");

			for (String line : lines)
			{
				parseLine(line);
			}
		}
		else
		{
			Log.w("ObjParser", "Couldn't read data. Aborting.");
		}

		fillBuffers();
	}

	private void parseLine(String line)
	{
		DebugUtil.logD("ObjParser", line, Constants.DEBUG_LEVEL_MORE_VERBOSE);

		String[] split = line.split(" ");

		if (split[0].equals("v"))
		{
			//vertice
			vertexBuffer.add(Float.valueOf(split[1]));
			vertexBuffer.add(Float.valueOf(split[2]));
			vertexBuffer.add(Float.valueOf(split[3]));

			//for simplicity, add a solid color (textures not handled... yet ;))
			colorsBuffer.add(1f); //r
			colorsBuffer.add(1f); //g
			colorsBuffer.add(1f); //b
			colorsBuffer.add(1f); //a
		}
		else if (split[0].equals("f"))
		{
			/*
			 * FIXME
			 * parse line to get attributes (normals, textures ...)
			 * We don't need these informations for now (cf previous comment)
			 */
			DebugUtil.logD("ObjParser", "Face information detected, but ignored for now", Constants.DEBUG_LEVEL_STANDARD);
		}
		else
		{
			//Not implemented yet, just print the line and ignore it
			DebugUtil.logD("ObjParser", "Unknown line : " + line + ", ignoring", Constants.DEBUG_LEVEL_STANDARD);
		}
	}

	/*
	 * Fill the buffers from the ArrayList previously filled
	 */
	private void fillBuffers()
	{
		numberOfVertices = vertexBuffer.size() / 3;

		DebugUtil.logD("ObjParser", "Number of vertices = " + numberOfVertices, Constants.DEBUG_LEVEL_STANDARD);

		// Initialize buffers
		mVertexBuffer = ByteBuffer.allocateDirect(numberOfVertices * 3 * 4)
				.order(ByteOrder.nativeOrder()).asFloatBuffer();
		mVertexBuffer.clear();
		mVertexBuffer.position(0);

		mColorsBuffer = ByteBuffer.allocateDirect(numberOfVertices * 4 * 4)
				.order(ByteOrder.nativeOrder()).asFloatBuffer();
		mColorsBuffer.clear();
		mColorsBuffer.position(0);

		mIndexBuffer = ByteBuffer.allocateDirect(numberOfVertices * 2)
				.order(ByteOrder.nativeOrder()).asShortBuffer();
		mIndexBuffer.clear();
		mIndexBuffer.position(0);

		// Fill 'em !
		for (Float f : vertexBuffer)
			mVertexBuffer.put(f);

		for (Float f : colorsBuffer)
			mColorsBuffer.put(f);

		for (short i = 0; i < numberOfVertices; ++i)
			mIndexBuffer.put(i);
	}

	/*
	 * -------------
	 * Getters
	 * -------------
	 */
	public FloatBuffer getVertexBuffer()
	{
		return mVertexBuffer;
	}

	public FloatBuffer getColorBuffer()
	{
		return mColorsBuffer;
	}

	public ShortBuffer getIndexBuffer()
	{
		return mIndexBuffer;
	}

	public int getBufferLength()
	{
		/*
		 * The index buffer length is the "base size".
		 * Multiply the result by 3 (x, y, z) to get the vertex buffer length
		 * Multiply the result by 4 (r, g, b, a) to get the color buffer length
		 *
		 * Don't forget to check if the result is positive (parsing suceeded) or not
		 * before using the returned value.
		 */
		return numberOfVertices;
	}
}
