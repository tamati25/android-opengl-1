package com.tama.labo.interfaces;

/**
 * Created by tama on 04/08/14.
 */
public interface IPredicate<T>
{
    public boolean check(T object);
}
