package com.tama.labo.interfaces;

/**
 * Created by tama on 23/07/14.
 *
 * Call a method on all objects (see World#applyAll method, called in MyRenderer#onSurfaceCreated)
 */
public interface IMap<T>
{
    //The method to apply
    public void apply(T obj);
}
