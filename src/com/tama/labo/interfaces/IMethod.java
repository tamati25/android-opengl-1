package com.tama.labo.interfaces;

import com.tama.labo.shader.Shader;

/**
 * Created by tama on 26/07/14.
 *
 * Some code that will be ran on an object
 */
public interface IMethod<T>
{
    /**
     * Run a code on an object
     * @param object the object
     */
    public void run(T object);
}
