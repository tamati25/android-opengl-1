package com.tama.labo.interfaces;

import com.tama.labo.attributes.Attribute;

/**
 * Created by fpan on 15/09/14.
 */
public interface IAttrChanged
{
    //Will be called when an attribute value is changed
    void onDataChanged(Integer index, Attribute attr);
}
