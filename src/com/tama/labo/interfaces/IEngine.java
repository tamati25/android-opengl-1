package com.tama.labo.interfaces;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.view.MotionEvent;

import com.tama.labo.game.Game;

public interface IEngine extends Runnable
{
    /**
     * This method is called before starting the engine, use this to initialize engine.
     * Return a list of shapes corresponding to the scene to be drawn before the engine is started,
     * or null if the engine doesn't need a scene.
     */
	public void init(Game game);

    /**
     * This method is called to set special GL parameters when using this engine (blending,
     * depth test ...)
     * It will be called just before drawing the scene for the first time by the renderer.
     */
	public void setGLParameters(GL10 p1, EGLConfig p2);

	/**
	 * This method is called when the engine is shut down to free any resource created
	 */
	public void finish();

    /**
     * Handle motion events (called from the surface view)
     * Returns true if the events was consumed
     */
	public boolean onTouch(Game game, MotionEvent e);

    /**
     * Handle motion events after the view has handled them
     * Returns true if the events was consumed
     */
	public boolean onTouchAfter(Game game, MotionEvent e);
}
