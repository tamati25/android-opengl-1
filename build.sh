#! /bin/bash

# Custom build script, for different configurations (debug/release)
# This script edits the line "boolean isDebugMode" in the DebugUtil
# class, setting it to false or true according to the given
# configuration.

# Usage : ./build <debug|release>

debugUtilClassName="DebugUtil"
debugUtilFilePath="src/com/tama/labo/util/${debugUtilClassName}.java"
debugLayoutFileName="debug_main.xml"
debugLayoutFilePath="res/layout/"

tmpFolder="/tmp/tama/android-opengl-1"
configuration="debug" # default is debug mode

if [ "$1" = "release" ]; then
    mkdir -p "$tmpFolder"

    cp "$debugUtilFilePath" "${tmpFolder}/${debugUtilClassName}.java"
    mv "$debugLayoutFilePath" "${tmpFolder}/${debugLayoutFileName}"

    sed -i 's/isDebugMode = true/isDebugMode = false/g' "$debugUtilFilePath"

    ant release

    mv "${tmpFolder}/${debugUtilClassName}.java" "$debugUtilFilePath"
    mv "${tmpFolder}/${debugLayoutFileName}" "$debugLayoutFilePath"
    rm -rf "$tmpFolder"

    exit 0
fi

# argument given was "debug", none, or unknown (ignored) -> debug mode

# explicitly set debug mode to true
sed -i 's/boolean isDebugMode .*/boolean isDebugMode = true;/g' "$debugUtilFilePath"
ant debug

exit 0
